## ProceduralTextures
### Description
OSX application for procedural texture generating using QuartzComposer-like graph builder. The graph is implemented on JS and based on the `flow` project: https://github.com/Ames/flow. Image generating and effects are implemented on Metal.
![screenshot](screenshot.png)


### Build
Just build in Xcode.

> ***Note!*** you need to setup the following `Capabilities`: `App Sandbox` with `Incoming/Outcoming network` and `User Selected File = Read/Write`.


### Functionality
- Texture generating arranged as a graph of nodes
- Multiple types of nodes 
    - `generator` - Generates patterned signal
    - `adjustment` - Performs color adjustments (i.e. levels, inverse, posterization).
    - `filter` - Image filters and distortions (i.e. bevel, soften, bump).
    - `mixer` - Mixer two images (i.e. alpha blending).
    - `common` - General nodes (i.e. result, transform, slider).
- Resulting image is exported to a grayscale file with resolution 1024x1024 pixels.
- Loading and saving graph to a JSON-file.
- Preview generated images.

### Graph creation
1. To add a new node just click on a corresponding item in the left menu (you may need to expand the necessary group first). Then drag it to a desired location.
2. To connect two nodes drag one's output to another one's input. The limitation is:
    - If input and output of objects do not impose values then you can connect them only if they have same names (or same first chars)
3. The result will be computed and represented only if the Result node exists. You can only have single instance of the result node.
4. Image can be imported by using node `Image`. 

***Note:*** 
- All other nodes work only with grayscaled images, so you must grayscale your image using the `Grayscale` node.
- As the graph produces texture, all images are processed as square.


### Hotkeys
- `⌥+⌘+P` - show preview window.
- `d` - duplicate selected nodes.
- `c` - copy selected nodes.
- `v` - paste nodes from buffer.
- `x` - cut selected nodes.
- `Del`/`⌫` - remove selected nodes.
### Shaders sandbox
Here is a directory `shaders_sandbox` contains shaders projects. To view and edit that projects you should use the KodeLife app from here: https://hexler.net/software/kodelife/

### How to add new filter
1. Add a new class `FilterName` with `TextureEffect` base class (that operation adds `FilterName.m` and `FilterName.h` files).
2. Add additional `FilterName.metal` and `FilterName_params.h` files to corresponding group.
3. In `FilterName.h` add `NSNumber` properties for numeric parameters of the filter. For example:
```objc
@property NSNumber *contrast;
```
4. In `FilterName.m` implement the following methods:
- May be your filter need some object inputs (i.e. texture), so you need to add it as a private parameter, for example:
```objc
//...
@implementation ContrastAdjustment
{
    id<MTLTexture> _imgTexture;
}
//...
```
- `initInputs` - here you must add to inputs dictionary parameters of your filter (includeing non-numeric), for example:
```objc
- (void)initInputs {
    [super initInputs];
    // Important: key name must be the same as filter input name.
    [self.inputs setValue:nil forKey:@"contrast"];
    [self.inputs setValue:nil forKey:@"img"];
    _contrast = @(0);
}
```
- `loadFragmentFunctionFromLibrary` - here you must load your fragment shader, for example:
```objc
- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshContrastAdjustment"];
}
```
- `setupCustomParams` - here you send values to your shaders, for example:
```objc
- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    // That parameters are defined in FilterName_params.h.
    ContrastAdjustmentParams params;
    params.contrast = [self getParamValue:@"contrast"];
    params.contrast = tanf(M_PI_4 + M_PI_4 * params.contrast * 0.9999);
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(ContrastAdjustmentParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}
```
- `eval` - a function that runs that filter, it can return any object including texture, for example:
```objc
- (id)eval:(id)arg {
    // {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    // } You should use that code in each your custom eval reimplementations
    // to use cached result of that node instead of expensive recomputing.
    
    // If we don't have any input object, we use existing empty texture.
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}
```
5. Define numeric inputs of your filter in `FilterName_params.h`, for example:
```objc
typedef struct
{
    float contrast;
} ContrastAdjustmentParams;
```
6. Write your shader in `FilterName.metal`, don't forget include headers:
```cpp
#include <metal_stdlib>
using namespace metal;
#include "../TECommon.metal"
#include "ContrastAdjustment_params.h"

fragment float4 fshContrastAdjustment(ColorInOut in [[stage_in]],
                                      texture2d<float, access::sample> inImg [[ texture(0) ]],
                                      constant ContrastAdjustmentParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::repeat, filter::linear);
    float val = inImg.sample(s, in.bkgCoord).x;
    val = (val - 0.5) * params.contrast + 0.5;
    
    return float4(clamp(val, 0.0, 1.0));
}
```
7. Add your filter creating code in `TextureEffectFabric.m`, for example:
```objc
    else if ([name isEqualToString:@"adjustment_contrast"]) {
        return [[ContrastAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
```
8. Now you need only to add description of your filter to JS code in `flow-types.js` file, for example:
```javascript
//...
{   
    type:'adjustment_contrast', // The same as in TextureEffectFabric.m.
    title:'Contrast',
    group:'adjustment',
    i:{
        transform:{}, // Common input for all filters.

        img:{}, // Object input image, the same as in FilterName.m.
        contrast:0 // Numeric input, the same as in FilterName.m and FilterName.h.
    },
    o:{img:{}} // Object output image.
},
//...
```
