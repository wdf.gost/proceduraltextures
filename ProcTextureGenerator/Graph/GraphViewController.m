//
//  GraphViewController.m
//  ProcTextureGenerator
//
//  Created by Georgy Ostrobrod on 07/12/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "GraphViewController.h"
#import "../AppDelegate.h"
#import "../Effects/TextureEffectFabric.h"

#import "Nodes/ResultGraphNode.h"
#import "Nodes/TransformGraphNode.h"
#import "Nodes/SliderGraphNode.h"
#import "Nodes/ImageGraphNode.h"
#import "Nodes/LoopNode.h"



@implementation GraphViewController

- (NSURL *)appURL {
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *filePath = [mainBundle pathForResource:@"index" ofType:@"html"];
	return [NSURL fileURLWithPath:filePath];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_graphView.configuration.userContentController
     addScriptMessageHandler:self name:@"interOp"];
    
    NSURL *url = [self appURL];
    [_graphView loadFileURL:url allowingReadAccessToURL:[url URLByDeletingLastPathComponent]];
    
    AppDelegate *app = NSApp.delegate;
    app.graphController = self;
}



- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message
{
    NSLog(@"%@", message.body);
    NSDictionary *sentData = (NSDictionary*)message.body;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    [formatter setDecimalSeparator:@"."];
    
    AppDelegate *app = NSApp.delegate;
    
    if ([sentData[@"command"] isEqualToString:@"add_node"]) {
        NSNumber *curID = [formatter numberFromString: sentData[@"id"]];
        if ([sentData[@"type"] isEqualToString:@"result"]) {
            [app addRootNode:[ResultGraphNode new]
                       atKey:curID];
        }
        else if ([sentData[@"type"] isEqualToString:@"transform"]) {
            [app addNode:[TransformGraphNode new]
                   atKey:curID];
        }
        else if ([sentData[@"type"] isEqualToString:@"slider"]) {
            [app addNode:[SliderGraphNode new]
                   atKey:curID];
        }
        else if ([sentData[@"type"] isEqualToString:@"image"]) {
            [app addNode:[ImageGraphNode new]
                   atKey:curID];
        }
        else if ([sentData[@"type"] isEqualToString:@"loop"]) {
            [app addNode:[LoopNode new]
                   atKey:curID];
        }
        else {
            [app addNode:[TextureEffectFabric createTextureEffectByName:sentData[@"type"]]
                   atKey:curID];
        }
    }
    else if ([sentData[@"command"] isEqualToString:@"add_connection"]) {
        NSNumber *dstID = [formatter numberFromString: sentData[@"in_id"]];
        NSNumber *srcID = [formatter numberFromString: sentData[@"out_id"]];
        [app addConnectionToNode:dstID
                           input:sentData[@"in_name"]
                        fromNode:srcID];
    }
    else if ([sentData[@"command"] isEqualToString:@"set_value"]) {
        NSNumber *curID = [formatter numberFromString: sentData[@"id"]];
        NSNumber *curValue = [formatter numberFromString:sentData[@"value"]];
        [app setValue:curValue
               toNode:curID
                input:sentData[@"input"]];
    }
    else if ([sentData[@"command"] isEqualToString:@"remove_node"]) {
        NSNumber *curID = [formatter numberFromString: sentData[@"id"]];
        [app removeNodeAtKey:curID];
    }
    else if ([sentData[@"command"] isEqualToString:@"remove_connection"]) {
        NSNumber *dstID = [formatter numberFromString: sentData[@"in_id"]];
        [app removeConnectionFromNode:dstID
                                input:sentData[@"in_name"]];
    }
    else if ([sentData[@"command"] isEqualToString:@"save"]) {
        [app saveGraphJSON:sentData[@"scene"]];
    }
}



- (void)requestGraphJSON {
    [_graphView evaluateJavaScript:@"document.saveLocal()"
                 completionHandler:^(NSString *result, NSError *error)
    {
        NSLog(@"Error %@",error);
        NSLog(@"Result %@",result);
    }];
}



- (void)setGraphJSON:(NSString *)json {
    [_graphView evaluateJavaScript:[NSString stringWithFormat:@"document.openLocal(\'%@\')", json]
                 completionHandler:^(NSString *result, NSError *error)
    {
        NSLog(@"Error %@",error);
        NSLog(@"Result %@",result);
    }];
}



- (void)clearGraph {
    [_graphView evaluateJavaScript:@"document.clearScene()"
                 completionHandler:^(NSString *result, NSError *error)
    {
        NSLog(@"Error %@",error);
        NSLog(@"Result %@",result);
    }];
}

@end
