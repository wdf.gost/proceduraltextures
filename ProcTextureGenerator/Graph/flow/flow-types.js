// Based on https://github.com/Ames/flow

var types_groups=[
    'common',
    'generator',
    'adjustment',
    'filter',
    'mixer'
]



var types_colors={
    'common':{fill:'#454535', stroke:'#6C6C5C'},
    'generator':{fill:'#453535', stroke:'#6C5C5C'},
    'adjustment':{fill:'#354535', stroke:'#5C6C5C'},
    'filter':{fill:'#353545', stroke:'#5C5C6C'},
    'mixer':{fill:'#352535', stroke:'#5C4C5C'}
}



var types=[
{   
    type:'result',
    title:'Result',
    group:'common',
    i:{img:{}}
},
{   
    type:'image',
    title:'Image',
    group:'common',
    o:{img:{}}
},
{   
    type:'loop',
    title:'Loop',
    group:'common',
    i:{
        iters:0,
        imgInit:{},
        imgLoop:{}
    },
    o:{
        imgRes:{},
        imgLoop:{}
    }
},
{   
    type:'transform',
    title:'Transform',
    group:'common',
    i:{
        a:0,
        tx:0,
        ty:0,
        sx:1,
        sy:1
    },
    o:{transform:{}}
},
{   
    type:'slider',
    title:'Slider',
    group:'common',
    i:{
        cur_val:0,
        min_val:-1,
        max_val:1,
        step:0.001
    },
    o:{val:0},
    init:function(i,o,that){
        var inp=document.createElement('input');
        inp.type='range';
        
        inp.setRange=function(min,max,step){
            var tmp=this.value;
            this.min=min;
            this.max=max;
            this.step=step;
            this.value=tmp;
        }
        inp.oninput=function(e){
            that.inputs['cur_val'].src.val=inp.value;
            that.outputs['val'].val=inp.value;

            console.log("Set " + that.id + ":cur_val to " + inp.value)
            send_webkit_msg({
                    "command": "set_value",
                    "id": String(that.id),
                    "input": String('cur_val'),
                    "value": String(inp.value)
                }) 

            that.widget.upLabels();
        }
        inp.setRange(i.min_val,i.max_val,i.step);
        inp.value=i.cur_val;

        that.widget.box.appendChild(inp);
        that.widget.resize();
        that.inp=inp;
    },
    update:function(that){
        console.log('Update slider')
        that.inp.setRange(that.inputs['min_val'].src.val,
                          that.inputs['max_val'].src.val,
                          that.inputs['step'].src.val);
        that.inp.value=that.inputs['cur_val'].src.val;
    }
},
{   
    type:'generator_chessboard',
    title:'Chessboard',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_value_noise',
    title:'Value noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_white_noise',
    title:'White noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_maze',
    title:'Maze',
    group:'generator',
    i:{
        transform:{},

        stepNum:10,
        weight:0.25
    },
    o:{img:{}}
},
{   
    type:'generator_splatters',
    title:'Splatters',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_triangle_noise',
    title:'Triangle noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_perlin_value_noise',
    title:'Perlin value noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10,
        octaves:8,
        persistance:0.5
    },
    o:{img:{}}
},
{   
    type:'generator_simplex_noise',
    title:'Simplex noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10
    },
    o:{img:{}}
},
{   
    type:'generator_perlin_simplex_noise',
    title:'Perlin simplex noise',
    group:'generator',
    i:{
        transform:{},

        stepNum:10,
        octaves:8,
        persistance:0.5
    },
    o:{img:{}}
},
{   
    type:'generator_value',
    title:'Value',
    group:'generator',
    i:{
        transform:{},

        value:0.5
    },
    o:{img:{}}
},
{   
    type:'generator_gradient',
    title:'Gradient',
    group:'generator',
    i:{
        transform:{},

        start:0.0,
        end:1.0
    },
    o:{img:{}}
},
{   
    type:'adjustment_contrast',
    title:'Contrast',
    group:'adjustment',
    i:{
        transform:{},

        img:{},
        contrast:0
    },
    o:{img:{}}
},
{   
    type:'adjustment_inverse',
    title:'Inverse',
    group:'adjustment',
    i:{
        transform:{},

        img:{}
    },
    o:{img:{}}
},
{   
    type:'adjustment_levels',
    title:'Levels',
    group:'adjustment',
    i:{
        transform:{},

        img:{},
        black:0,
        white:0,
        gamma:0
    },
    o:{img:{}}
},
{   
    type:'adjustment_posterize',
    title:'Posterize',
    group:'adjustment',
    i:{
        transform:{},

        img:{},
        color_num:16
    },
    o:{img:{}}
},
{   
    type:'adjustment_grayscale',
    title:'Grayscale',
    group:'adjustment',
    i:{
        transform:{},

        img:{},
    },
    o:{img:{}}
},
{   
    type:'filter_bevel',
    title:'Bevel',
    group:'filter',
    i:{
        transform:{},

        img:{},
        angleOffset:0
    },
    o:{img:{}}
},
{   
    type:'filter_soften',
    title:'Soften',
    group:'filter',
    i:{
        transform:{},

        img:{}
    },
    o:{img:{}}
},
{   
    type:'filter_sharpen',
    title:'Sharpen',
    group:'filter',
    i:{
        transform:{},

        img:{}
    },
    o:{img:{}}
},
{   
    type:'filter_bump',
    title:'Bump',
    group:'filter',
    i:{
        transform:{},

        img:{},
        radius:100,
        amount:1
    },
    o:{img:{}}
},
{   
    type:'filter_pinch',
    title:'Pinch',
    group:'filter',
    i:{
        transform:{},

        img:{},
        radius:100,
        amount:1
    },
    o:{img:{}}
},
{   
    type:'filter_twirl',
    title:'Twirl',
    group:'filter',
    i:{
        transform:{},

        img:{},
        radius:100,
        angle:1
    },
    o:{img:{}}
},
{   
    type:'filter_sobel_field',
    title:'Sobel Field',
    group:'filter',
    i:{
        transform:{},

        img:{}
    },
    o:{img:{}}
},
{   
    type:'filter_normal_blur',
    title:'Normal Blur',
    group:'filter',
    i:{
        transform:{},

        imgImg:{},
        imgMap:{},
        sigma:1.0
    },
    o:{img:{}}
},
{   
    type:'filter_tangent_blur',
    title:'Tangent Blur',
    group:'filter',
    i:{
        transform:{},

        imgImg:{},
        imgMap:{},
        sigma:1.0
    },
    o:{img:{}}
},
{   
    type:'filter_xdog',
    title:'xDoG',
    group:'filter',
    i:{
        transform:{},

        imgG1:{},
        imgG2:{},
        epsilon:0.5,
        phi:10.0,
        p:0.5
    },
    o:{img:{}}
},
{   
    type:'mixer_alpha_blend',
    title:'Alpha Blend',
    group:'mixer',
    i:{
        transform:{},

        imgFrg:{},
        imgBkg:{},
        alpha:1.0,
        blendMode:0
    },
    o:{img:{}}
}
];



var nodeTypes={};

function makeTypes(){
	for(var i in types){
		nodeType(types[i]);
	}
}



function nodeType(dArg){
	nodeTypes[dArg.type]=function NodeType(iArg){
		this.inherit=Node;
		this.inherit(dArg,iArg,free_id++);
	}
}


