// Based on https://github.com/Ames/flow

var selectStart;
var selectBox;
var selected=[];

function makeLibrary(){
	
	var newTxt = ''
	for (var jj in types_groups) {
		newTxt+='<button class="accordion">' + types_groups[jj] + '</button>';
		newTxt+='<div class="panel">'
		for(var ii in types){
			if (types[ii].group == types_groups[jj])
				newTxt+='<li><a href="#" onmousedown="dragging=[makeNode({type:\''+types[ii].type+'\',x:event.clientX+120,y:event.clientY-10})];return false;" onclick="return false;" title="">'+types[ii].title+'</a></li>'
		}
		newTxt+='</div>'
	}
	
	document.getElementById('library').innerHTML=newTxt;	

	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
	    /* Toggle between adding and removing the "active" class,
	    to highlight the button that controls the panel */
	    this.classList.toggle("active");

	    /* Toggle between hiding and showing the active panel */
	    var panel = this.nextElementSibling;
	    if (panel.style.display === "block") {
	      panel.style.display = "none";
	    } else {
	      panel.style.display = "block";
	    }
	  });
	}
}



document.saveLocal = function saveLocal() {
	selectAll()
	send_webkit_msg({
        "command": "save",
        "scene": JSON.stringify(exportNodes(selected))
    })
    deselect()
}



document.openLocal = function openLocal(scene) {
	loadScene(JSON.parse(scene));
}



document.clearScene = function clearScene() {
	selectAll()
	deleteNodes(selected);
}



function loadScene(scn){
	selectAll()
	deleteNodes(selected);
	importNodes(scn);
	selectAll()
	updateInputs(selected)
	deselect()
}



function updateInputs(theNodes) {
	for(var ii in theNodes){
		var n=theNodes[ii];
		for (var jj in n.inputs) {
			if (typeof(n.inputs[jj].src.val) == 'object') {
				continue;;
			}
			send_webkit_msg({
					            "command": "set_value",
					            "id": String(n.id),
					            "input": String(jj),
					            "value": String(n.inputs[jj].src.val)
					        })
		}    					
	}
}



function selectAll() {
	deselect()
	for(var ii in nodes){
		var n=nodes[ii];
		select(n);
	}
}



//function plotWidget
document.onkeydown=function keyDown(e){
	//console.log(e);
	
	if(e.target.type=="textarea")
		return;

	if(e.target.type=="text")
		return;
		
	switch(e.which){
		case 8: //backspace
		case 46: //delete
			e.preventDefault();
			deleteNodes(selected);
			break;
		case 68: //d
			duplicate();
			break;
		case 67: //c
			copyNodes(selected);
			break;
		case 86: //v
			pasteNodes();
			break;
		case 88: //x
			cutNodes(selected);
			break;
	}
}



document.onmousedown=function mouseDown(e){
	mouse=getMouse(e);
	if(e.target.id=='container' ||e.target.nodeName== 'HTML'){ // probably won't work in ff
		if(!selectBox){
			selectBox=document.createElement('div');
			canvDiv.appendChild(selectBox);
			selectBox.className='selectBox';
		}
		selectStart=getMouse(e);
		doSelectBox();
	}	
}



function select(node){
	selected.push(node);
	node.widget.redraw();
}



function deselect(node){
	if(node){
		selected.splice(selected.indexOf(node),1);	
		node.widget.redraw();
	}else{
		for(var ii=selected.length-1;ii>=0;ii--){
			deselect(selected[ii]);
		}
	}
}



document.onmouseup=function mouseUp(e){
	mouse=getMouse(e);

	dragging=null;
	if(wiring){
		wiring[2].remove();
		wiring=null;
	}
	if(selectStart){
		doSelectBox();
		selectStart=false;
		selectBox.style.visibility='hidden';
	}
}



document.onmousemove=function mouseMove(e){
	
	newMouse=getMouse(e);

	if(window.dragging){
		var dx=newMouse[0]-mouse[0];
		var dy=newMouse[1]-mouse[1];
		for(var ii in dragging){
			dragging[ii].widget.x+=dx;
			dragging[ii].widget.y+=dy;
			dragging[ii].widget.upLoc();
		}
	}
	if(window.wiring){
		wiring[3][0]=newMouse[0];
		wiring[3][1]=newMouse[1];
		wiring[2].redraw();
	}
	if(window.selectStart){
		selectBox.style.width=Math.abs(newMouse[0]-selectStart[0])+'px';
		selectBox.style.height=Math.abs(newMouse[1]-selectStart[1])+'px';
		selectBox.style.left=Math.min(newMouse[0],selectStart[0])+'px';
		selectBox.style.top=Math.min(newMouse[1],selectStart[1])+'px';
		selectBox.style.visibility='visible';
		doSelectBox();
	}	
	
	mouse=newMouse;
}



document.onblur=function(){
	dragging=null;
	if(selectStart){
		selectStart=false;
		selectBox.style.visibility='hidden';
	}
};



function doSelectBox(){
	var xMin=Math.min(mouse[0],selectStart[0])
	var xMax=Math.max(mouse[0],selectStart[0])
	var yMin=Math.min(mouse[1],selectStart[1])
	var yMax=Math.max(mouse[1],selectStart[1])
	
	for(var ii in nodes){
		var n=nodes[ii];
		var w=n.widget;
		var over=w.x<xMax && w.x+w.w>xMin && w.y<yMax && w.y+w.h>yMin;
		
		if(over && selected.indexOf(n)==-1){
			select(n);
		}else if(!over && selected.indexOf(n)!=-1){
			deselect(n);
		}
	}
}



function deleteNodes(nodes){
	for(var i=nodes.length-1;i>=0;i--){
		var n=nodes[i]

		send_webkit_msg({
	            "command": "remove_node",
	            "id": String(n.id)
	        })

		deselect(n);
		window.nodes[n.id].remove();
		delete window.nodes[n.id];
	}
}



function cutNodes(nodes){
	copyNodes(nodes);
	deleteNodes(nodes);
}



function copyNodes(nodes){
	var xNodes=exportNodes(nodes);
	for(var i in xNodes.nodes){
		xNodes.nodes[i].x-=mouse[0];
		xNodes.nodes[i].y-=mouse[1];
	}
	localStorage.setItem('pasteboard',JSON.stringify(xNodes));
}



function pasteNodes(){
	
	var xNodes=JSON.parse(localStorage.getItem('pasteboard'))
	
	for(var i in xNodes.nodes){
		xNodes.nodes[i].x+=mouse[0];
		xNodes.nodes[i].y+=mouse[1];
	}
	var newNodes=importNodes(xNodes);
	
	deselect();
	
	for(var i in newNodes){
		select(newNodes[i]);
	}
}



function exportNodes(theNodes){
	var nBuf=[];
	var wBuf=[];
	
	console.log('Export nodes')

	for(var ii in theNodes){
		var n=theNodes[ii];
		nBuf.push(n.doExport());
		for(var n1 in n.widget.inWires){
			var w=n.widget.inWires[n1];
			
			for(var jj in theNodes){
				var n2=theNodes[jj];
				for(var n2o in n2.widget.outWires){
					for(var n2oi in n2.widget.outWires[n2o]){
						if(w==n2.widget.outWires[n2o][n2oi]){
							wBuf.push({n1:jj,p1:n2o,n2:ii,p2:n1,color:w.color});
							send_webkit_msg({
						            "command": "add_connection",
						            "out_id": String(n1.id),
						            "out_name": String(n2o),
						            "in_id": String(n2.id),
						            "in_name": String(n1)
						        })
						}
					}
				}
			}
		}
	}
	return {nodes:nBuf,wires:wBuf};
}



function importNodes(parcel){
	
	var newNodes=[];
	for(var iii in parcel.nodes){
		newNodes[iii]=makeNode(parcel.nodes[iii]);
	}	
	for(var ii in parcel.wires){
		var w=parcel.wires[ii];
		mkWire(newNodes[w.n1],w.p1,newNodes[w.n2],w.p2,w.color);
	}
	return newNodes;
}



function makeNode(o){
	if (o.type == 'result') {
		if (result_existed)
			return
		else 
			result_existed=true
	}

	send_webkit_msg({
            "command": "add_node",
            "type": String(o.type),
            "id": String(free_id)
        })
	return new nodeTypes[o.type](o);
}



function duplicate(){

	var buf=exportNodes(selected);

	deselect();
	
	var newNodes=importNodes(buf);

	for(var ii in newNodes){
		select(newNodes[ii]);
	}
	
	dragging=selected;
}



function getMouse(e) {
	var posx = 0;
	var posy = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY) 	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY) 	{
		posx = e.clientX + document.body.scrollLeft
			+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop
			+ document.documentElement.scrollTop;
	}
	return [posx,posy];
}
