// Based on https://github.com/Ames/flow

var Node=function Node(dArg,iArg,id_val){
	
	
	this.id=id_val;
	this.type=dArg.type;
	this.group=dArg.group;
	
	var i={};
	for(var ii in dArg.i) i[ii]=dArg.i[ii];
	for(var ii in iArg.i) i[ii]=iArg.i[ii];
	this.inputs={};
    for(var ii in i) {
		this.inputs[ii]={src:{val:i[ii]}};
    }

	var o={};
	for(var ii in dArg.o) o[ii]=dArg.o[ii];
	for(var ii in iArg.o) o[ii]=iArg.o[ii];
	this.outputs={};
    for(var ii in o) {
		this.outputs[ii]={node:this,val:o[ii]};
    }

	var widget=new Widget(this);
	widget.title=iArg.title||dArg.title||"";
	widget.resize();
    if(iArg.x && iArg.y) {
		widget.upLoc(iArg.x,iArg.y);
    }
	this.widget=widget;
	
	var initF=dArg.init||null;
	if(initF){
		initF(i,o,this);
	}

	var updateF=dArg.update||function(){};
	this.update=function(){
		updateF(this)
	}


	this.doExport=function(){
		var obj={};
		obj.type=this.type;
		obj.title=this.widget.title;
		obj.x=this.widget.x;
		obj.y=this.widget.y;
		obj.i={};
		for(var ii in this.inputs){
			var val=this.inputs[ii].src.val;
			
			if(typeof(val) != 'object' && !isFinite(val)){
			  val=val.toString();
			}
			 
			obj.i[ii]=val;
		}
		
		return obj;
	}


	this.draw=function(){
        if(dArg.draw) {
			dArg.draw(i,o,this);
        }
		this.widget.upLabels();
	}

	
	this.remove=function(){
		
		if(dArg.remove)
			dArg.remove(i,o,this);
		
		for(var ii in this.widget.inWires){
			rmWire(this,ii);
		};
		
		for(var n in nodes){
			var nod=nodes[n];
			for(var inp in nod.inputs){
				if(nod.inputs[inp].src.node && nod.inputs[inp].src.node==this){
					rmWire(nod,inp);	
				} 
			}
		}
		if (this.type == "result")
			result_existed=false
		
		this.widget.remove();
	}
	
	
	this.widget.upLoc();
	this.widget.redraw();
	nodes[this.id] = this;
}
