// Based on https://github.com/Ames/flow

var canvDiv;

var nodes={};
var free_id=0;

var dragging=false;
var mouse=[0,0];
var wiring;

var result_existed=false;



function init(){
	canvDiv=document.getElementById('container');

	makeTypes();
	
    (function animloop(){
      requestAnimFrame(animloop);
      draw();
    })();

	var tmpScene=localStorage.getItem('tmpScene');
	if(tmpScene){
	   loadScene(JSON.parse(tmpScene));
	}
	makeLibrary();
}



function send_webkit_msg(msg_body) {
    try {
        window.webkit.messageHandlers.interOp.postMessage(msg_body)
    } catch {
        console.log(JSON.stringify(msg_body))
    }
}



var draw=function draw(){
	for(var ii in nodes){
		nodes[ii].draw();
	}
}



function unloading(){
	localStorage.setItem('tmpScene',JSON.stringify(exportNodes(nodes)));	
}



Math.dist=function(x1,y1,x2,y2){
	return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));	
}



objectSize = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};



window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame || 
    window.webkitRequestAnimationFrame || 
    window.mozRequestAnimationFrame    || 
    window.oRequestAnimationFrame      || 
    window.msRequestAnimationFrame     || 
    function(/* function */ callback, /* DOMElement */ element){
      window.setTimeout(callback, 1000 / 60);
    };
})();
