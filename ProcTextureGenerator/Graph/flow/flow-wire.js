// Based on https://github.com/Ames/flow

var Wire=function Wire(p1,p2,color,dst){
	
	var element=document.createElement('canvas');
	canvDiv.appendChild(element);	
	var ctx;
	element.style['pointer-events']='none';
	element.style.zIndex='-1';
	
	var thick=3.3;
	
	var margin=4;
	
	this.p1=p1;
	this.p2=p2;

	this.color=color;
	this.dst_id = dst

	this.redraw=function(){
		
		var x=Math.min(p1[0],p2[0])-margin;
		var y=Math.min(p1[1],p2[1])-margin;
		
		var w=Math.abs(p1[0]-p2[0])+margin*2;
		var h=Math.abs(p1[1]-p2[1])+margin*2;
		
		element.style.top=y;
		element.style.left=x;
		element.width=w;
		element.height=h;

		var half=w/3*((p1[1]>p2[1])?1:2);
		
		ctx = element.getContext('2d');
		
		ctx.strokeStyle=color;
		ctx.lineCap='round';
		ctx.lineWidth=thick;
		ctx.beginPath();
		ctx.moveTo(p1[0]-x,p1[1]-y);
		ctx.bezierCurveTo(half,p1[1]-y,
						  half,p2[1]-y,
						  p2[0]-x,p2[1]-y);
		
		ctx.stroke();
	}
	
	this.remove=function(){
		canvDiv.removeChild(element);
	}
}



function mkWire(n1,p1,n2,p2,color){
	if(!n1.outputs[p1] || !n2.inputs[p2]) 
		return;

	if (typeof(n1.outputs[p1].val) == 'object') {
		if (typeof(n2.inputs[p2].src.val) == 'object') {
			if(p1.substring(0,3) != p2.substring(0,3))
		        return;
		} else {
			return
		}
	}
	
	if(isFinite(color))
		color='#5C5C5C';
	
	n2.inputs[p2].src=n1.outputs[p1];
	n2.inputs[p2].srcPort=p1;
	
	var newWire=new Wire(n2.inputs[p2].pt,n1.outputs[p1].pt,color,n2.id);
	
	n2.widget.inWires[p2]=newWire;
	
	if(!n1.widget.outWires[p1])
		n1.widget.outWires[p1]=[];
		
	n1.widget.outWires[p1].push(newWire);
	
	send_webkit_msg({
            "command": "add_connection",
            "out_id": String(n1.id),
            "out_name": String(p1),
            "in_id": String(n2.id),
            "in_name": String(p2)
        })
	
	n1.widget.redraw();
	n2.widget.redraw();
	newWire.redraw();
}



function rmWire(n2,p2){
	if(!n2.inputs[p2]) return;
	
	var n1=n2.inputs[p2].src.node;
	var p1=n2.inputs[p2].srcPort;
	
	var wire=n2.widget.inWires[p2]
	delete n2.widget.inWires[p2];
	
	wire.remove();
	
	for(var ii in n1.widget.outWires[p1]){
		if(n1.widget.outWires[p1][ii]==wire){
			n1.widget.outWires[p1].splice(ii,1);
		}
	}

	send_webkit_msg({
            "command": "remove_connection",
            "out_id": String(n1.id),
            "out_name": String(p1),
            "in_id": String(n2.id),
            "in_name": String(p2)
        })
	
	n2.inputs[p2].src={val:n2.inputs[p2].src.val};
	n1.widget.redraw();
	n2.widget.redraw();
}
