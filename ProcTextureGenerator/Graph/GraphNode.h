//
//  GraphNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 10/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



@interface GraphNode : NSObject

- (void)addConnectionFromNode:(GraphNode *)node toInput:(NSString *)input;
- (void)removeConnectionAtInput:(NSString *)input;

- (id)eval:(id __nullable)arg;

- (bool)checkAndResetDependedOn:(GraphNode *)node;
- (bool)checkAndResetDependedOn:(GraphNode *)node until:(GraphNode *)finNode;
- (bool)checkAndResetDependedOn:(GraphNode *)node untilNext:(GraphNode *)finNode;
- (id)cachedValue;
- (void)resetNode;
- (void)resetNodeUntil:(GraphNode *)node;
- (void)resetNodeUntilNext:(GraphNode *)node;
- (void)setCachedValue:(id __nullable)value;
- (void)setCacheModeOn:(bool)cached until:(GraphNode *)node;
- (void)setCacheModeOn:(bool)cached untilNext:(GraphNode *)node;


// private:
- (NSMutableDictionary<NSString *, GraphNode *> *)inputs;
- (void)initInputs;


@end

NS_ASSUME_NONNULL_END
