//
//  GraphViewController.h
//  ProcTextureGenerator
//
//  Created by Georgy Ostrobrod on 07/12/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>



NS_ASSUME_NONNULL_BEGIN

@interface GraphViewController : NSViewController <WKScriptMessageHandler>

@property (weak) IBOutlet WKWebView *graphView;

- (void)requestGraphJSON;
- (void)setGraphJSON:(NSString *)json;
- (void)clearGraph;


@end

NS_ASSUME_NONNULL_END
