//
//  ResultGraphNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 10/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "GraphNode.h"



NS_ASSUME_NONNULL_BEGIN

@interface ResultGraphNode : GraphNode

@end

NS_ASSUME_NONNULL_END
