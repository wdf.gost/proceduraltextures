//
//  TransformGraphNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "TransformGraphNode.h"

@implementation TransformGraphNode

- (void)initInputs {
    [self.inputs setValue:nil forKey:@"a"];
    [self.inputs setValue:nil forKey:@"tx"];
    [self.inputs setValue:nil forKey:@"ty"];
    [self.inputs setValue:nil forKey:@"sx"];
    [self.inputs setValue:nil forKey:@"sy"];
    
    _a = @(0);
    _tx = @(0);
    _ty = @(0);
    _sx = @(1);
    _sy = @(1);
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    NSNumber *a = self.inputs[@"a"] == nil ? _a : [self.inputs[@"a"] eval:arg];
    NSNumber *tx = self.inputs[@"tx"] == nil ? _tx : [self.inputs[@"tx"] eval:arg];
    NSNumber *ty = self.inputs[@"ty"] == nil ? _ty : [self.inputs[@"ty"] eval:arg];
    NSNumber *sx = self.inputs[@"sx"] == nil ? _sx : [self.inputs[@"sx"] eval:arg];
    NSNumber *sy = self.inputs[@"sy"] == nil ? _sy : [self.inputs[@"sy"] eval:arg];
    
    return @{@"a":a,
             @"tx":tx,
             @"ty":ty,
             @"sx":sx,
             @"sy":sy};
}

@end
