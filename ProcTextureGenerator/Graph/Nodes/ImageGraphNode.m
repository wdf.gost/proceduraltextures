//
//  ImageGraphNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//
#import <Metal/Metal.h>
#import <Accelerate/Accelerate.h>
#import <Cocoa/Cocoa.h>

#import "ImageGraphNode.h"



@implementation ImageGraphNode
{
    id<MTLTexture> _imgTexture;
}



- (vImage_Buffer)bufferFromImage:(NSImage * __nullable)image {
    
    NSBitmapImageRep* rep = (NSBitmapImageRep *)[[image representations] objectAtIndex: 0];
    NSSize size = NSMakeSize(rep.pixelsWide, rep.pixelsHigh);
    
    if (!image) {
        return (vImage_Buffer) {0, 0, 0, 0};
    }
    
    vImage_Buffer buffer = {
        malloc(size.height * size.width * 4),
        size.height,
        size.width,
        size.width * 4
    };
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef contextBg = CGBitmapContextCreate(buffer.data,
                                                   buffer.width,
                                                   buffer.height,
                                                   8,
                                                   buffer.rowBytes,
                                                   colorSpace,
                                                   kCGImageAlphaPremultipliedLast);
    
    [NSGraphicsContext saveGraphicsState];
    [image setSize: size];
    [NSGraphicsContext setCurrentContext:[NSGraphicsContext graphicsContextWithCGContext:contextBg flipped:NO]];
    [image drawInRect:NSMakeRect(0, 0, buffer.width, buffer.height)
             fromRect:NSZeroRect
            operation:NSCompositingOperationCopy
             fraction:1.0];
    [NSGraphicsContext restoreGraphicsState];
    
    CGContextRelease(contextBg);
    CGColorSpaceRelease(colorSpace);
    
    return buffer;
}



- (void)loadTexture {
    NSOpenPanel *openDlg = [NSOpenPanel openPanel];
    openDlg.canChooseFiles = YES;
    openDlg.canChooseDirectories = NO;
    openDlg.allowsMultipleSelection = NO;
    openDlg.allowedFileTypes = @[@"jpeg", @"jpg",@"png", @"bmp", @"tiff"];
    
    if ([openDlg runModal] != NSModalResponseOK) {
        return;
    }
    
    NSImage *image = [[NSImage alloc] initWithContentsOfURL:[openDlg URLs][0]];
    vImage_Buffer imageBuf = [self bufferFromImage:image];
    
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    
    MTLTextureDescriptor *textureDescriptor =
    [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm
                                                       width:imageBuf.width
                                                      height:imageBuf.height
                                                   mipmapped:NO];
    textureDescriptor.storageMode = MTLStorageModeManaged;
    textureDescriptor.usage       = MTLTextureUsageShaderRead;
    _imgTexture = [device newTextureWithDescriptor:textureDescriptor];
    
    id<MTLCommandQueue> queue = [device newCommandQueue];
    id<MTLCommandBuffer> commandBuffer = [queue commandBuffer];
    
    [_imgTexture replaceRegion:MTLRegionMake2D(0, 0, imageBuf.width, imageBuf.height)
                   mipmapLevel:0
                     withBytes:imageBuf.data
                   bytesPerRow:imageBuf.rowBytes];
    
    [commandBuffer commit];
    [commandBuffer waitUntilCompleted];
    
    free(imageBuf.data);
}



- (void)initInputs {
    
}



- (id)eval:(id)arg {
    if (_imgTexture == nil) {
        [self loadTexture];
    }
    return _imgTexture;
}

@end
