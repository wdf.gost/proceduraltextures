//
//  ResultGraphNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 10/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//
#import <Metal/Metal.h>

#import "ResultGraphNode.h"



@implementation ResultGraphNode

- (void)initInputs {
    [self.inputs setValue:nil forKey:@"img"];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    id<MTLTexture> img = self.inputs[@"img"] == nil ? nil : [self.inputs[@"img"] eval:arg];
    
    [self setCachedValue:img];
    
    return img;
}

@end
