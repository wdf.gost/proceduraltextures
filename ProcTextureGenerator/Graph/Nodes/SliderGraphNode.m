//
//  SliderGraphNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "SliderGraphNode.h"

@implementation SliderGraphNode

- (void)initInputs {
    [self.inputs setValue:nil forKey:@"cur_val"];
    [self.inputs setValue:nil forKey:@"min_val"];
    [self.inputs setValue:nil forKey:@"max_val"];
    [self.inputs setValue:nil forKey:@"step"];
    
    _cur_val = @(0);
    _min_val = @(-1);
    _max_val = @(1);
    _step = @(0.001);
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    NSNumber *val = self.inputs[@"cur_val"] == nil ? _cur_val : [self.inputs[@"cur_val"] eval:arg];
    
    return val;
}

@end
