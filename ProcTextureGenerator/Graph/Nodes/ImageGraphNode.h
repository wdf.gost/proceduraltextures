//
//  ImageGraphNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "GraphNode.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageGraphNode : GraphNode


@end

NS_ASSUME_NONNULL_END
