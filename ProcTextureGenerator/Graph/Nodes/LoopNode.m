//
//  LoopNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 23/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//
#import <Metal/Metal.h>

#import "LoopNode.h"

@implementation LoopNode
{
    size_t _curIter;
    id<MTLTexture> _img;
}



- (void)initInputs {
    [self.inputs setValue:nil forKey:@"imgInit"];
    [self.inputs setValue:nil forKey:@"imgLoop"];
    [self.inputs setValue:nil forKey:@"iters"];
    _iters = @(0);
    
    _curIter = 0;
}



- (bool)checkAndResetDependedOn:(GraphNode *)node {
    _curIter = 0;
    return [super checkAndResetDependedOn:node untilNext:self];
}



- (void)resetNode {
    _curIter = 0;
    [super resetNodeUntilNext:self];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    
    NSNumber *iters = self.inputs[@"iters"] == nil ? _iters : [self.inputs[@"iters"] eval:arg];
    if (_curIter == 0) {
        _img = self.inputs[@"imgInit"] == nil ? nil : [self.inputs[@"imgInit"] eval:arg];
        [self setCacheModeOn:NO untilNext:self];
    }
    
    _curIter++;
    if (_curIter > iters.intValue) {
        _curIter = 0;
    } else {
        [self resetNodeUntilNext:self];
        _img = self.inputs[@"imgLoop"] == nil ? nil : [self.inputs[@"imgLoop"] eval:arg];
    }
    [self setCachedValue:_img];
    
    return _img;
}


@end
