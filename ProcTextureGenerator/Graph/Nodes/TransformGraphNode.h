//
//  TransformGraphNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "../GraphNode.h"



NS_ASSUME_NONNULL_BEGIN

@interface TransformGraphNode : GraphNode

@property NSNumber *a;
@property NSNumber *tx;
@property NSNumber *ty;
@property NSNumber *sx;
@property NSNumber *sy;

@end

NS_ASSUME_NONNULL_END
