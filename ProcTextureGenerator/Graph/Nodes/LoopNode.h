//
//  LoopNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 23/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "GraphNode.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoopNode : GraphNode

@property NSNumber *iters;

@end

NS_ASSUME_NONNULL_END
