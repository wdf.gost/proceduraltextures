//
//  SliderGraphNode.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//
#import "../GraphNode.h"



NS_ASSUME_NONNULL_BEGIN

@interface SliderGraphNode : GraphNode

@property NSNumber *cur_val;
@property NSNumber *min_val;
@property NSNumber *max_val;
@property NSNumber *step;

@end

NS_ASSUME_NONNULL_END
