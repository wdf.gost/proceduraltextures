//
//  GraphNode.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 10/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "GraphNode.h"



@implementation GraphNode
{
    NSMutableDictionary<NSString *, GraphNode *> *_inputs;
    id _cache;
    bool _cached;
}



- (NSMutableDictionary<NSString *,GraphNode *> *)inputs {
    return _inputs;
}



- (instancetype)init {
    self = [super init];
    if (self) {
        _inputs = [NSMutableDictionary new];
        _cached = YES;
        [self initInputs];
    }
    return self;
}



- (void)initInputs {
    NSAssert(NO, @"Don't use base class.");
}



- (id)cachedValue {
    if (_cached) {
        return _cache;
    }
    return nil;
}



- (void)resetNode {
    _cache = nil;
    for (id key in _inputs) {
        [_inputs[key] resetNode];
    }
}



- (void)resetNodeUntil:(GraphNode *)node {
    if (self == node) {
        return;
    }
    _cache = nil;
    for (id key in _inputs) {
        [_inputs[key] resetNodeUntil:node];
    }
}



- (void)resetNodeUntilNext:(GraphNode *)node {
    _cache = nil;
    for (id key in _inputs) {
        [_inputs[key] resetNodeUntil:node];
    }
}



- (void)setCachedValue:(id)value {
    _cache = value;
}



- (id)eval:(id)arg {
    NSAssert(NO, @"Don't use base class.");
    if (_cache && _cached) {
        return _cache;
    }
    return nil;
}



- (void)addConnectionFromNode:(GraphNode *)node
                      toInput:(NSString *)input
{
    _inputs[input] = node;
}



- (void)removeConnectionAtInput:(NSString *)input {
    if ([_inputs objectForKey:input] == nil) {
        return;
    }
    
    _inputs[input] = nil;
}



- (bool)checkAndResetDependedOn:(GraphNode *)node {
    if (self == node) {
        [self setCachedValue:nil];
        return YES;
    }
    
    BOOL res = NO;
    for(id key in _inputs) {
        res |= [_inputs[key] checkAndResetDependedOn:node];
        if (res) {
            [self setCachedValue:nil];
        }
    }
    return res;
}



- (bool)checkAndResetDependedOn:(GraphNode *)node until:(GraphNode *)finNode {
    if (self == node) {
        [self setCachedValue:nil];
        return YES;
    }
    if (self == finNode) {
        return NO;
    }
    
    BOOL res = NO;
    for(id key in _inputs) {
        res |= [_inputs[key] checkAndResetDependedOn:node until:finNode];
        if (res) {
            [self setCachedValue:nil];
        }
    }
    return res;
}



- (bool)checkAndResetDependedOn:(GraphNode *)node untilNext:(GraphNode *)finNode {
    if (self == node) {
        [self setCachedValue:nil];
        return YES;
    }
    
    BOOL res = NO;
    for(id key in _inputs) {
        res |= [_inputs[key] checkAndResetDependedOn:node until:finNode];
        if (res) {
            [self setCachedValue:nil];
        }
    }
    return res;
}



- (void)setCacheModeOn:(bool)cached until:(GraphNode *)node {
    if (self == node) {
        return;
    }
    _cached = cached;
    for (id key in _inputs) {
        [_inputs[key] setCacheModeOn:cached until:node];
    }
}



- (void)setCacheModeOn:(bool)cached untilNext:(GraphNode *)node {
    for (id key in _inputs) {
        [_inputs[key] setCacheModeOn:cached until:node];
    }
}


@end
