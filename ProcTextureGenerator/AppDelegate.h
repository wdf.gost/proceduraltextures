//
//  AppDelegate.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Metal/Metal.h>

#import "GraphNode.h"
#import "Graph/GraphViewController.h"



@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) GraphViewController *graphController;


- (void)addNode:(GraphNode *)node atKey:(NSNumber *)key;
- (void)addRootNode:(GraphNode *)node atKey:(NSNumber *)key;
- (void)removeNodeAtKey:(NSNumber *)key;
- (void)addConnectionToNode:(NSNumber *)dstID
                      input:(NSString *)dstInput
                   fromNode:(NSNumber *)srcID;
- (void)removeConnectionFromNode:(NSNumber *)dstID
                           input:(NSString *)dstInput;
- (void)setValue:(NSNumber *)value
          toNode:(NSNumber *)key
           input:(NSString *)input;

- (id)computeGraphIn:(id<MTLCommandBuffer>)cmdBuf;


- (IBAction)onExportImage:(id)sender;
- (IBAction)onNewGraph:(id)sender;
- (IBAction)onSaveGraph:(id)sender;
- (IBAction)onLoadGraph:(id)sender;

- (void)saveGraphJSON:(NSString *)json;
- (void)updateGraph;

@end
