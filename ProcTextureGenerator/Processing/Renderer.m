//
//  Renderer.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <simd/simd.h>
#import <ModelIO/ModelIO.h>

#import "Renderer.h"
#import "../AppDelegate.h"

// Include header shared between C code here, which executes Metal API commands, and .metal files
#import "ShaderTypes.h"


static const NSUInteger kMaxBuffersInFlight = 3;

static const float GRID_VTC[8] = { -1,  2,    2,  2,    -1,  -1,    2,  -1};
static const float DEFAULT_VTC[8] = { 0,  1,    1,  1,    0,  0,    1,  0};
static const float DEFAULT_VTP[8] = {-1,  1,    1,  1,   -1, -1,    1, -1};
static const size_t DEFAULT_COORDS_SIZE = sizeof(float) * 8;



@implementation Renderer
{
    dispatch_semaphore_t _inFlightSemaphore;
    id <MTLDevice> _device;
    id <MTLCommandQueue> _commandQueue;
    
    id <MTLRenderPipelineState> _pipelineState;
    
    id<MTLBuffer> _vBufPosition;
    id<MTLBuffer> _vBufTexture;
    id<MTLBuffer> _vBufTextureGrid;
    
    id<MTLTexture> _texture;
    
    float _ratio;
}



-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view;
{
    self = [super init];
    if(self)
    {
        _device = view.device;
        _inFlightSemaphore = dispatch_semaphore_create(kMaxBuffersInFlight);
        [self _loadMetalWithView:view];
        [self _loadAssets];
    }
    
    return self;
}



- (void)_loadMetalWithView:(nonnull MTKView *)view;
{
    /// Load Metal state objects and initalize renderer dependent view properties
    
    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm;
    view.sampleCount = 1;
    
    id<MTLLibrary> defaultLibrary = [_device newDefaultLibrary];
    id <MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vertexShader"];
    id <MTLFunction> fragmentFunction = [defaultLibrary newFunctionWithName:@"fragmentShader"];
    
    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"MyPipeline";
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipelineStateDescriptor.colorAttachments[0].blendingEnabled = NO;
    
    NSError *error = NULL;
    _pipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
    if (!_pipelineState)
    {
        NSLog(@"Failed to created pipeline state, error %@", error);
    }
    
    _commandQueue = [_device newCommandQueue];
}



- (void)_loadAssets
{
    /// Load assets into metal objects
    
    
    MTLResourceOptions resourceOptions = (MTLResourceOptionCPUCacheModeDefault |
                                          MTLStorageModeShared);
    
    _vBufPosition = [_device newBufferWithBytes:DEFAULT_VTP
                                         length:DEFAULT_COORDS_SIZE
                                        options:resourceOptions];
    _vBufTexture  = [_device newBufferWithBytes:DEFAULT_VTC
                                         length:DEFAULT_COORDS_SIZE
                                        options:resourceOptions];
    _vBufTextureGrid  = [_device newBufferWithBytes:GRID_VTC
                                             length:DEFAULT_COORDS_SIZE
                                            options:resourceOptions];
}



- (void)drawInMTKView:(nonnull MTKView *)view
{
    /// Per frame updates here
    
    dispatch_semaphore_wait(_inFlightSemaphore, DISPATCH_TIME_FOREVER);
    
    id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
    commandBuffer.label = @"MyCommand";
    
    __block dispatch_semaphore_t block_sema = _inFlightSemaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer)
     {
         dispatch_semaphore_signal(block_sema);
     }];
    
    /// Delay getting the currentRenderPassDescriptor until we absolutely need it to avoid
    ///   holding onto the drawable and blocking the display pipeline any longer than necessary
    MTLRenderPassDescriptor* renderPassDescriptor = view.currentRenderPassDescriptor;
    
    if(renderPassDescriptor != nil) {
        
        AppDelegate *app = NSApp.delegate;
        _texture = [app computeGraphIn:commandBuffer];
        
        /// Final pass rendering code here
        
        id <MTLRenderCommandEncoder> renderEncoder =
        [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
        renderEncoder.label = @"MyRenderEncoder";
        
        [renderEncoder pushDebugGroup:@"DrawBox"];
        [renderEncoder setRenderPipelineState:_pipelineState];
        
        [renderEncoder setVertexBuffer:_vBufPosition
                                offset:0
                               atIndex:0];
        if (_useGrid) {
            [renderEncoder setVertexBuffer:_vBufTextureGrid
                                    offset:0
                                   atIndex:1];
        }
        else {
            [renderEncoder setVertexBuffer:_vBufTexture
                                    offset:0
                                   atIndex:1];
        }
        float ratio[] = {
            _ratio > 1.0 ? 1.0 / _ratio : 1.0,
            _ratio < 1.0 ? _ratio : 1.0
        };
        [renderEncoder setVertexBytes:ratio
                               length:8
                              atIndex:2];
        
        [renderEncoder setFragmentTexture:_texture atIndex:0];
        
        [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip
                          vertexStart:0
                          vertexCount:4];
        
        [renderEncoder popDebugGroup];
        
        [renderEncoder endEncoding];
        
        [commandBuffer presentDrawable:view.currentDrawable];
    }
    
    [commandBuffer commit];
}



- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
    /// Respond to drawable size or orientation changes here
    
    _ratio = size.width / (float)size.height;
}

@end
