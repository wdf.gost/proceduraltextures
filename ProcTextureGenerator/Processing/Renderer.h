//
//  Renderer.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <MetalKit/MetalKit.h>

#import "../Effects/TextureEffect.h"



// Our platform independent renderer class.   Implements the MTKViewDelegate protocol which
//   allows it to accept per-frame update and drawable resize callbacks.
@interface Renderer : NSObject <MTKViewDelegate>

-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view;

@property BOOL useGrid;

@end

