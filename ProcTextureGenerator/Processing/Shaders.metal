//
//  Shaders.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

// File for Metal kernel and shader functions

#include <metal_stdlib>
#include <simd/simd.h>

// Including header shared between this Metal shader code and Swift/C code executing Metal API commands
#import "ShaderTypes.h"

using namespace metal;





typedef struct
{
    float4 position [[position]];
    float2 texCoord;
} ColorInOut;



vertex ColorInOut vertexShader(constant float2 *vtp  [[ buffer(0) ]],
                               constant float2 *vtc  [[ buffer(1) ]],
                               constant float2 &ratio [[ buffer(2) ]],
                               uint vid [[ vertex_id ]])
{
    ColorInOut out;
    
    float4 position = float4(vtp[vid].x * ratio.x, -vtp[vid].y * ratio.y, 0.0, 1.0);
    out.position = position;
    out.texCoord = vtc[vid];
    
    return out;
}



fragment float4 fragmentShader(ColorInOut in [[stage_in]],
                               texture2d<float, access::sample> srcTexture [[ texture(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::nearest);
    
    return float4(srcTexture.sample(s, in.texCoord).xyz, 1.0);
}
