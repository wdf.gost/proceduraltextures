//
//  GameViewController.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "Renderer.h"

// Our macOS view controller.
@interface GameViewController : NSViewController

- (void)turnOnGridDrawing:(BOOL)on;

@end
