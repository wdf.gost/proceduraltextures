//
//  TextureEffect
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>

#import "../Graph/GraphNode.h"



NS_ASSUME_NONNULL_BEGIN



@interface TextureEffect : GraphNode

- (instancetype)initWithDevice:(id<MTLDevice>)device;


// Protected section.
- (float)getParamValue:(NSString *)key;


// Private section.
- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library;
- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder;
- (id<MTLTexture>)stubTexture;


@end

NS_ASSUME_NONNULL_END
