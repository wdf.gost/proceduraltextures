//
//  LevelsAdjustment.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "LevelsAdjustment_params.h"



fragment float4 fshLevelsAdjustment(ColorInOut in [[stage_in]],
                                        texture2d<float, access::sample> inImg [[ texture(0) ]],
                                        constant LevelsAdjustmentParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float val = inImg.sample(s, in.bkgCoord).x;
    val = (val - params.black) / (1.0 + params.white - params.black);
    val = pow(clamp(val, 0.0, 1.0), exp(-params.gamma));
    
    return float4(clamp(val, 0.0, 1.0));
}



