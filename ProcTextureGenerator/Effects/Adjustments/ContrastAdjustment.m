//
//  ContrastAdjustment.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "ContrastAdjustment.h"
#import "ContrastAdjustment_params.h"



@implementation ContrastAdjustment
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"contrast"];
    [self.inputs setValue:nil forKey:@"img"];
    _contrast = @(0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshContrastAdjustment"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    ContrastAdjustmentParams params;
    params.contrast = [self getParamValue:@"contrast"];
    params.contrast = tanf(M_PI_4 + M_PI_4 * params.contrast * 0.9999);
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(ContrastAdjustmentParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
