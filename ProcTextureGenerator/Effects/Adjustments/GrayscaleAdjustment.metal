//
//  GrayscaleAdjustment.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"



fragment float4 fshGrayscaleAdjustment(ColorInOut in [[stage_in]],
                                     texture2d<float, access::sample> inImg [[ texture(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float val = dot(inImg.sample(s, in.bkgCoord).xyz,
                    float3(0.299, 0.587, 0.114));
    
    return float4(clamp(val, 0.0, 1.0));
}


