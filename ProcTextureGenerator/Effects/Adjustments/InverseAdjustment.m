//
//  InverseAdjustment.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "InverseAdjustment.h"

@implementation InverseAdjustment
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"img"];
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshInverseAdjustment"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
