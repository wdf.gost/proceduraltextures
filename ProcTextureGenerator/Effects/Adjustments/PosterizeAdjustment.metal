//
//  PosterizeAdjustment.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "PosterizeAdjustment_params.h"



fragment float4 fshPosterizeAdjustment(ColorInOut in [[stage_in]],
                                    texture2d<float, access::sample> inImg [[ texture(0) ]],
                                    constant PosterizeAdjustmentParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float val = inImg.sample(s, in.bkgCoord).x;
    
    val = round(val * params.color_num) / params.color_num;
    
    return float4(clamp(val, 0.0, 1.0));
}



