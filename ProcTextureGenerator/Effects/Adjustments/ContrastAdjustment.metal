//
//  ContrastAdjustment.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "ContrastAdjustment_params.h"



fragment float4 fshContrastAdjustment(ColorInOut in [[stage_in]],
                                      texture2d<float, access::sample> inImg [[ texture(0) ]],
                                      constant ContrastAdjustmentParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float val = inImg.sample(s, in.bkgCoord).x;
    val = (val - 0.5) * params.contrast + 0.5;
    
    return float4(clamp(val, 0.0, 1.0));
}


