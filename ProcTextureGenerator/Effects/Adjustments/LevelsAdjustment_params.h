//
//  LevelsAdjustment_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef LevelsAdjustment_params_h
#define LevelsAdjustment_params_h

typedef struct
{
    float white;
    float black;
    float gamma;
} LevelsAdjustmentParams;


#endif /* LevelsAdjustment_params_h */
