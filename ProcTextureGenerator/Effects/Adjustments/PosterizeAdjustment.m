//
//  PosterizeAdjustment.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "PosterizeAdjustment.h"
#import "PosterizeAdjustment_params.h"



@implementation PosterizeAdjustment
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"color_num"];
    [self.inputs setValue:nil forKey:@"img"];
    _color_num = @(16);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshPosterizeAdjustment"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    PosterizeAdjustmentParams params;
    params.color_num = [self getParamValue:@"color_num"] - 1;
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(PosterizeAdjustmentParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
