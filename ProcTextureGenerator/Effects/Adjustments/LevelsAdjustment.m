//
//  LevelsAdjustment.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "LevelsAdjustment.h"
#import "LevelsAdjustment_params.h"



@implementation LevelsAdjustment
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"black"];
    [self.inputs setValue:nil forKey:@"white"];
    [self.inputs setValue:nil forKey:@"gamma"];
    [self.inputs setValue:nil forKey:@"img"];
    _black = @(0);
    _white = @(0);
    _gamma = @(0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshLevelsAdjustment"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    LevelsAdjustmentParams params;
    params.black = [self getParamValue:@"black"];
    params.white = [self getParamValue:@"white"];
    params.gamma = [self getParamValue:@"gamma"];    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(LevelsAdjustmentParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
