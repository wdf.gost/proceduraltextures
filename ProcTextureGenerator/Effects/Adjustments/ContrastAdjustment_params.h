//
//  ContrastAdjustment_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef ContrastAdjustment_params_h
#define ContrastAdjustment_params_h

typedef struct
{
    float contrast;
} ContrastAdjustmentParams;


#endif /* ContrastAdjustment_params_h */
