//
//  PosterizeAdjustment_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef PosterizeAdjustment_params_h
#define PosterizeAdjustment_params_h

typedef struct
{
    int color_num;
} PosterizeAdjustmentParams;


#endif /* PosterizeAdjustment_params_h */
