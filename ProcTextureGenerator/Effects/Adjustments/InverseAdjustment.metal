//
//  InverseAdjustment.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"



fragment float4 fshInverseAdjustment(ColorInOut in [[stage_in]],
                                        texture2d<float, access::sample> inImg [[ texture(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float val = inImg.sample(s, in.bkgCoord).x;
    val = 1.0 - val;
    
    return float4(clamp(val, 0.0, 1.0));
}


