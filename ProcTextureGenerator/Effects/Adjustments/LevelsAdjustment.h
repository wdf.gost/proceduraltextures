//
//  LevelsAdjustment.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 29/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface LevelsAdjustment : TextureEffect

@property NSNumber *black;
@property NSNumber *white;
@property NSNumber *gamma;

@end

NS_ASSUME_NONNULL_END
