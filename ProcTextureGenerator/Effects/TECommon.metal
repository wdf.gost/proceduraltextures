//
//  TECommon.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
#include <simd/simd.h>

using namespace metal;


#define M_PI 3.141592653589793
#define FLOAT_EPS 0.0001


typedef struct
{
    float4 position [[position]];
    float2 texCoord;
    float2 bkgCoord;
} ColorInOut;



inline static float rand(float2 pos) {
    return fract(sin(dot(pos,
                         float2(12.9898, 78.233))) *
                 43758.5453123);
}



inline static float gauss_in_point(float l, float sigma)
{
    float s2 = sigma * sigma;
    return 1.0 / sqrt(2.0 * M_PI * s2) * exp(- l * l / (2.0 * s2));
}


/*
inline static float tanh(float x)
{
    float e = exp(2.0 * x);
    return (e - 1) / (e + 1);
}
*/
