//
//  PerlinSimplexNoiseGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef PerlinSimplexNoiseGenerator_params_h
#define PerlinSimplexNoiseGenerator_params_h

typedef struct
{
    int step_num;
    int octaves;
    float persistance;
} PerlinSimplexNoiseGeneratorParams;


#endif /* PerlinSimplexNoiseGenerator_params_h */
