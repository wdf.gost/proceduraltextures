//
//  GradientGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "GradientGenerator_params.h"



fragment float4 fshGradientGenerator(ColorInOut in [[stage_in]],
                                  constant GradientGeneratorParams &params [[ buffer(0) ]])
{
    float val = mix(params.start, params.end, fract(in.texCoord.x));
    
    return float4(clamp(val, 0.0, 1.0));
}

