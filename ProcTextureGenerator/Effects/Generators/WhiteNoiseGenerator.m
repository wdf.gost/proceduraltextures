//
//  WhiteNoiseGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "WhiteNoiseGenerator.h"
#import "WhiteNoiseGenerator_params.h"

@implementation WhiteNoiseGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    _stepNum = @(10);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshWhiteNoiseGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    WhiteNoiseGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(WhiteNoiseGeneratorParams)
                            atIndex:0];
}

@end
