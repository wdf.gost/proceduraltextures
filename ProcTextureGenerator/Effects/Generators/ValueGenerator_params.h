//
//  ValueGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef ValueGenerator_params_h
#define ValueGenerator_params_h

typedef struct
{
    float value;
} ValueGeneratorParams;


#endif /* ValueGenerator_params_h */
