//
//  GradientGenerator.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface GradientGenerator : TextureEffect

@property NSNumber *start;
@property NSNumber *end;

@end

NS_ASSUME_NONNULL_END
