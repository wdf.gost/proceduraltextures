//
//  ValueGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "ValueGenerator.h"
#import "ValueGenerator_params.h"



@implementation ValueGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"value"];
    [self.inputs setValue:nil forKey:@"persistance"];
    _value = @(0.5);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshValueGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    ValueGeneratorParams params;
    params.value = [self getParamValue:@"value"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(ValueGeneratorParams)
                            atIndex:0];
}

@end
