//
//  ValueNoiseGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef ValueNoiseGenerator_params_h
#define ValueNoiseGenerator_params_h

typedef struct
{
    int step_num;
} ValueNoiseGeneratorParams;


#endif /* ValueNoiseGenerator_params_h */
