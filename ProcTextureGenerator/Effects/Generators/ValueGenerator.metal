//
//  ValueGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "ValueGenerator_params.h"






fragment float4 fshValueGenerator(ColorInOut in [[stage_in]],
                                  constant ValueGeneratorParams &params [[ buffer(0) ]])
{
    float val = params.value;
    
    return float4(clamp(val, 0.0, 1.0));
}

