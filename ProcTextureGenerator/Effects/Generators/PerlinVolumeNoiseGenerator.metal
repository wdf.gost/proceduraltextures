//
//  PerlinVolumeNoiseGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "PerlinVolumeNoiseGenerator_params.h"



static float value_noise(float2 pos, int steps) {
    float2 pos_stepped = pos * steps;
    float2 pos_int = floor(pos_stepped);
    float2 pos_flt = fract(pos_stepped);
    
    float a = rand(pos_int);
    float b = rand(pos_int + float2(1.0, 0.0));
    float c = rand(pos_int + float2(0.0, 1.0));
    float d = rand(pos_int + float2(1.0, 1.0));
    
    float2 mix_coef = smoothstep(0.0, 1.0, pos_flt);
    
    return (mix(a, b, mix_coef.x) +
            (c - a) * mix_coef.y * (1.0 - mix_coef.x) +
            (d - b) * mix_coef.y * mix_coef.x);
}



static float perlin_noise(float2 pos, int steps, int octaves, float persistance) {
    float val = 0.5;
    float amp = 0.5;
    float2 cur_pos = pos;
    float inv_persistance = 1.0 / persistance;
    
    for (int i = 0; i < octaves; ++i) {
        float rand_val = value_noise(cur_pos, steps * inv_persistance * i) - 0.5;
        val += amp * rand_val;
        cur_pos *= inv_persistance;
        amp *= persistance;
    }
    
    return val;
}



fragment float4 fshPerlinVolumeNoiseGenerator(ColorInOut in [[stage_in]],
                                          constant PerlinVolumeNoiseGeneratorParams &params [[ buffer(0) ]])
{
    float2 pos = in.texCoord;
    
    float val = perlin_noise(pos, params.step_num, params.octaves, params.persistance);
    
    return float4(clamp(val, 0.0, 1.0));
}



