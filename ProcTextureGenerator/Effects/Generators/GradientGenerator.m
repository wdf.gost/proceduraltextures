//
//  GradientGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "GradientGenerator.h"
#import "GradientGenerator_params.h"



@implementation GradientGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"start"];
    [self.inputs setValue:nil forKey:@"end"];
    _start = @(0.0);
    _end = @(1.0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshGradientGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    GradientGeneratorParams params;
    params.start = [self getParamValue:@"start"];
    params.end = [self getParamValue:@"end"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(GradientGeneratorParams)
                            atIndex:0];
}

@end
