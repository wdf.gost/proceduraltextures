//
//  ChessboardGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "ChessboardGenerator_params.h"



static float chess(float2 pos, int steps) {
    int x_val = int(abs(pos.x) * steps + float(pos.x < 0)) % 2;
    int y_val = int(abs(pos.y) * steps + float(pos.y > 0)) % 2;
    return float((x_val + y_val) % 2);
}


fragment float4 fshChessboardGenerator(ColorInOut in [[stage_in]],
                                       constant ChessboardGeneratorParams &params [[ buffer(0) ]])
{
    float2 pos = in.texCoord;
    
    float val = chess(pos, params.step_num);
    
    return float4(clamp(val, 0.0, 1.0));
}
