//
//  TriangleNoiseGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef TriangleNoiseGenerator_params_h
#define TriangleNoiseGenerator_params_h

typedef struct
{
    int step_num;
} TriangleNoiseGeneratorParams;


#endif /* TriangleNoiseGenerator_params_h */
