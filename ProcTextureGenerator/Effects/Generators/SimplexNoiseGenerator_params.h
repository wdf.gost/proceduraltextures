//
//  SimplexNoiseGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef SimplexNoiseGenerator_params_h
#define SimplexNoiseGenerator_params_h

typedef struct
{
    int step_num;
} SimplexNoiseGeneratorParams;


#endif /* SimplexNoiseGenerator_params_h */
