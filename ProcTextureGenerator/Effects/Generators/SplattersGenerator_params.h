//
//  SplattersGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef SplattersGenerator_params_h
#define SplattersGenerator_params_h

typedef struct
{
    int step_num;
} SplattersGeneratorParams;



#endif /* SplattersGenerator_params_h */
