//
//  ValueNoiseGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "ValueNoiseGenerator.h"
#import "ValueNoiseGenerator_params.h"



@implementation ValueNoiseGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    _stepNum = @(10);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshValueNoiseGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    ValueNoiseGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(ValueNoiseGeneratorParams)
                            atIndex:0];
}

@end
