//
//  WhiteNoiseGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef WhiteNoiseGenerator_params_h
#define WhiteNoiseGenerator_params_h

typedef struct
{
    int step_num;
} WhiteNoiseGeneratorParams;


#endif /* WhiteNoiseGenerator_params_h */
