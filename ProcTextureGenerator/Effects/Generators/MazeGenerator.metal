//
//  MazeGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "MazeGenerator_params.h"



static float maze(float2 pos, int steps, float weight) {
    float2 scaled_pos = fract(pos * float2(steps));
    float index = rand(floor(pos * float2(steps)));
    float ind = fract((index - 0.5) * 2.0);
    
    float2 st = scaled_pos;
    if (ind > 0.75) {
        st = 1.0 - scaled_pos;
    }
    else if (ind > 0.5) {
        st = float2(1.0 - scaled_pos.x, scaled_pos.y);
    }
    else if (ind > 0.25) {
        st = float2(scaled_pos.x, 1.0 - scaled_pos.y);
    }
    
    return (smoothstep(st.x - weight, st.x, st.y) -
            smoothstep(st.x, st.x + weight, st.y));
}



fragment float4 fshMazeGenerator(ColorInOut in [[stage_in]],
                                       constant MazeGeneratorParams &params [[ buffer(0) ]])
{
    float2 pos = in.texCoord;
    
    float val = maze(pos, params.step_num, params.weight);
    
    return float4(clamp(val, 0.0, 1.0));
}






