//
//  TriangleNoiseGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "TriangleNoiseGenerator_params.h"


#define INV_SIN_60 1.1547
static float2 skew(float2 pos, float k) {
    float sx = k * pos.x;
    return float2(
                  sx,
                  pos.y + 0.5 * sx
                  );
}



static float triangle_noise(float2 pos, int steps) {
    float2 p = fract(skew(pos * steps, INV_SIN_60));
    float2 pos_int = floor(skew(pos * steps, INV_SIN_60));
    float k = float(p.x > p.y);
    return mix(rand(pos_int), rand(-pos_int),  k);
}



fragment float4 fshTriangleNoiseGenerator(ColorInOut in [[stage_in]],
                                      constant TriangleNoiseGeneratorParams &params [[ buffer(0) ]])
{
    float2 pos = in.texCoord;
    
    float val = triangle_noise(pos, params.step_num);
    
    return float4(clamp(val, 0.0, 1.0));
}
