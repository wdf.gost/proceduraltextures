//
//  ChessboardGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "ChessboardGenerator.h"
#import "ChessboardGenerator_params.h"



@implementation ChessboardGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    _stepNum = @(10);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshChessboardGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    ChessboardGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(ChessboardGeneratorParams)
                            atIndex:0];
}

@end
