//
//  MazeGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef MazeGenerator_params_h
#define MazeGenerator_params_h

typedef struct
{
    int step_num;
    float weight;
} MazeGeneratorParams;


#endif /* MazeGenerator_params_h */
