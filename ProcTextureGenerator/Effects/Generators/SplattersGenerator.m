//
//  SplattersGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "SplattersGenerator.h"
#import "SplattersGenerator_params.h"



@implementation SplattersGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    _stepNum = @(10);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshSplattersGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    SplattersGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(SplattersGeneratorParams)
                            atIndex:0];
}

@end
