//
//  WhiteNoiseGenerator.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface WhiteNoiseGenerator : TextureEffect

@property NSNumber *stepNum;

@end

NS_ASSUME_NONNULL_END
