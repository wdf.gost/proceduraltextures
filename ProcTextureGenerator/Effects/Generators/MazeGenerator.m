//
//  MazeGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "MazeGenerator.h"
#import "MazeGenerator_params.h"



@implementation MazeGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    [self.inputs setValue:nil forKey:@"weight"];
    _stepNum = @(10);
    _weight = @(0.25);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshMazeGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    MazeGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    params.weight = [self getParamValue:@"weight"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(MazeGeneratorParams)
                            atIndex:0];
}

@end
