//
//  PerlinSimplexNoiseGenerator.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface PerlinSimplexNoiseGenerator : TextureEffect

@property NSNumber *stepNum;
@property NSNumber *octaves;
@property NSNumber *persistance;

@end

NS_ASSUME_NONNULL_END
