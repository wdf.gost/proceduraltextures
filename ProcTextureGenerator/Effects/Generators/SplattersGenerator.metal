//
//  SplattersGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "SplattersGenerator_params.h"



static float value_noise(float2 pos, int steps) {
    float2 pos_stepped = pos * steps;
    float2 pos_int = floor(pos_stepped);
    float2 pos_flt = fract(pos_stepped);
    
    float a = rand(pos_int);
    float b = rand(pos_int + float2(1.0, 0.0));
    float c = rand(pos_int + float2(0.0, 1.0));
    float d = rand(pos_int + float2(1.0, 1.0));
    
    float2 mix_coef = smoothstep(0.0, 1.0, pos_flt);
    
    return (mix(a, b, mix_coef.x) +
            (c - a) * mix_coef.y * (1.0 - mix_coef.x) +
            (d - b) * mix_coef.y * mix_coef.x);
}



static float splatters(float2 pos, int steps) {
    float value = 0.0;
    value += smoothstep(0.15, 0.2, value_noise(pos, steps));
    value -= smoothstep(0.35, 0.4, value_noise(pos, steps));
    return value;
}



fragment float4 fshSplattersGenerator(ColorInOut in [[stage_in]],
                                 constant SplattersGeneratorParams &params [[ buffer(0) ]])
{
   float2 pos = in.texCoord;
    
    float val = splatters(pos, params.step_num);
    
    return float4(clamp(val, 0.0, 1.0));
}
