//
//  WhiteNoiseGenerator.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "WhiteNoiseGenerator_params.h"



static float white_noise(float2 pos, int steps) {
    float2 scaled_pos = floor(pos * float2(steps));
    return rand(scaled_pos);
}



fragment float4 fshWhiteNoiseGenerator(ColorInOut in [[stage_in]],
                                       constant WhiteNoiseGeneratorParams &params [[ buffer(0) ]])
{
    float2 pos = in.texCoord;
    
    float val = white_noise(pos, params.step_num);
    
    return float4(clamp(val, 0.0, 1.0));
}






