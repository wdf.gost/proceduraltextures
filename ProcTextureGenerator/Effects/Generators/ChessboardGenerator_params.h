//
//  ChessboardGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef ChessboardGenerator_params_h
#define ChessboardGenerator_params_h

typedef struct
{
    int step_num;
} ChessboardGeneratorParams;

#endif /* ChessGenerator_params_h */
