//
//  GradientGenerator_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef GradientGenerator_params_h
#define GradientGenerator_params_h

typedef struct
{
    float start;
    float end;
} GradientGeneratorParams;


#endif /* GradientGenerator_params_h */
