//
//  PerlinVolumeNoiseGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "PerlinVolumeNoiseGenerator.h"
#import "PerlinVolumeNoiseGenerator_params.h"



@implementation PerlinVolumeNoiseGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    [self.inputs setValue:nil forKey:@"octaves"];
    [self.inputs setValue:nil forKey:@"persistance"];
    _stepNum = @(10);
    _octaves = @(8);
    _persistance = @(0.5);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshPerlinVolumeNoiseGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    PerlinVolumeNoiseGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    params.octaves = [self getParamValue:@"octaves"];
    params.persistance = [self getParamValue:@"persistance"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(PerlinVolumeNoiseGeneratorParams)
                            atIndex:0];
}

@end
