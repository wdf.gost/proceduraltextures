//
//  TriangleNoiseGenerator.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "TriangleNoiseGenerator.h"
#import "TriangleNoiseGenerator_params.h"



@implementation TriangleNoiseGenerator

- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"stepNum"];
    _stepNum = @(10);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshTriangleNoiseGenerator"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    TriangleNoiseGeneratorParams params;
    params.step_num = [self getParamValue:@"stepNum"];
    
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(TriangleNoiseGeneratorParams)
                            atIndex:0];
}

@end
