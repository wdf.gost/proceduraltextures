//
//  TextureEffectFabric.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import <Metal/Metal.h>

#import "TextureEffectFabric.h"

#import "Generators/ChessboardGenerator.h"
#import "Generators/ValueNoiseGenerator.h"
#import "Generators/WhiteNoiseGenerator.h"
#import "Generators/MazeGenerator.h"
#import "Generators/SplattersGenerator.h"
#import "Generators/TriangleNoiseGenerator.h"
#import "Generators/PerlinVolumeNoiseGenerator.h"
#import "Generators/SimplexNoiseGenerator.h"
#import "Generators/PerlinVolumeNoiseGenerator.h"
#import "Generators/ValueGenerator.h"
#import "Generators/GradientGenerator.h"

#import "Adjustments/ContrastAdjustment.h"
#import "Adjustments/InverseAdjustment.h"
#import "Adjustments/LevelsAdjustment.h"
#import "Adjustments/PosterizeAdjustment.h"
#import "Adjustments/GrayscaleAdjustment.h"

#import "Filters/BevelFilter.h"
#import "Filters/SoftenFilter.h"
#import "Filters/SharpenFilter.h"
#import "Filters/BumpFilter.h"
#import "Filters/PinchFilter.h"
#import "Filters/TwirlFilter.h"
#import "Filters/SobelFieldFilter.h"
#import "Filters/NormalBlurFilter.h"
#import "Filters/TangentBlurFilter.h"
#import "Filters/xDoGFilter.h"

#import "Mix/AlphaBlendMixer.h"



@implementation TextureEffectFabric

+ (GraphNode *)createTextureEffectByName:(NSString *)name {
    // ------ Generators ------
    if ([name isEqualToString:@"generator_chessboard"]) {
        return [[ChessboardGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_value_noise"]) {
        return [[ValueNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_white_noise"]) {
        return [[WhiteNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_maze"]) {
        return [[MazeGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_splatters"]) {
        return [[SplattersGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_triangle_noise"]) {
        return [[TriangleNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_perlin_value_noise"]) {
        return [[PerlinVolumeNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_simplex_noise"]) {
        return [[SimplexNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_perlin_simplex_noise"]) {
        return [[PerlinVolumeNoiseGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_value"]) {
        return [[ValueGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"generator_gradient"]) {
        return [[GradientGenerator alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    
    // ------ Adjustments ------
    else if ([name isEqualToString:@"adjustment_contrast"]) {
        return [[ContrastAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"adjustment_inverse"]) {
        return [[InverseAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"adjustment_levels"]) {
        return [[LevelsAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"adjustment_posterize"]) {
        return [[PosterizeAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"adjustment_grayscale"]) {
        return [[GrayscaleAdjustment alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    
    // ------ Filters ------
    else if ([name isEqualToString:@"filter_bevel"]) {
        return [[BevelFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_soften"]) {
        return [[SoftenFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_sharpen"]) {
        return [[SharpenFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_bump"]) {
        return [[BumpFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_pinch"]) {
        return [[PinchFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_twirl"]) {
        return [[TwirlFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_sobel_field"]) {
        return [[SobelFieldFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_normal_blur"]) {
        return [[NormalBlurFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_tangent_blur"]) {
        return [[TangentBlurFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    else if ([name isEqualToString:@"filter_xdog"]) {
        return [[xDoGFilter alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    
    // ------ Filters ------
    else if ([name isEqualToString:@"mixer_alpha_blend"]) {
        return [[AlphaBlendMixer alloc] initWithDevice:MTLCreateSystemDefaultDevice()];
    }
    
    
    
    return nil;
}

@end
