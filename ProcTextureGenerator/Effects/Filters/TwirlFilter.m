//
//  TwirlFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "TwirlFilter.h"
#import "TwirlFilter_params.h"



@implementation TwirlFilter
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"radius"];
    [self.inputs setValue:nil forKey:@"angle"];
    [self.inputs setValue:nil forKey:@"img"];
    _radius = @(100);
    _angle = @(1);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshTwirlFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    TwirlFilterParams params;
    params.pixel_step = 1.0 / _imgTexture.width;
    params.radius = [self getParamValue:@"radius"];
    params.angle = [self getParamValue:@"angle"] * M_PI;
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(TwirlFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
