//
//  xDoGFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef xDoGFilter_params_h
#define xDoGFilter_params_h

typedef struct
{
    float epsilon;
    float phi;
    float p;
} xDoGFilterParams;


#endif /* xDoGFilter_params_h */
