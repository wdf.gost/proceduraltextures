//
//  SoftenFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef SoftenFilter_params_h
#define SoftenFilter_params_h

typedef struct
{
    float pixel_step; // We assume ratio is 1.
} SoftenFilterParams;


#endif /* SoftenFilter_params_h */
