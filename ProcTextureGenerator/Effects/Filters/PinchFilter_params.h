//
//  PinchFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef PinchFilter_params_h
#define PinchFilter_params_h

typedef struct
{
    float radius;
    float amount;
    float pixel_step; // We assume ratio is 1.
} PinchFilterParams;


#endif /* PinchFilter_params_h */
