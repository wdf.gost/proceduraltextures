//
//  xDoGFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "xDoGFilter.h"
#import "xDoGFilter_params.h"



@implementation xDoGFilter
{
    id<MTLTexture> _g1Texture;
    id<MTLTexture> _g2Texture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"imgG1"];
    [self.inputs setValue:nil forKey:@"imgG2"];
    [self.inputs setValue:nil forKey:@"epsilon"];
    [self.inputs setValue:nil forKey:@"phi"];
    [self.inputs setValue:nil forKey:@"p"];
    _epsilon = @(0.5);
    _phi = @(10.0);
    _p = @(0.5);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshXDOGFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    xDoGFilterParams params;
    params.epsilon = [self getParamValue:@"epsilon"];
    params.phi = [self getParamValue:@"phi"];
    params.p = [self getParamValue:@"p"];
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(xDoGFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_g1Texture
                              atIndex:0];
    [renderEncoder setFragmentTexture:_g2Texture
                              atIndex:1];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _g1Texture = (self.inputs[@"imgG1"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"imgG1"] eval:arg]);
    
    _g2Texture = (self.inputs[@"imgG2"] == nil ?
                   self.stubTexture :
                  [self.inputs[@"imgG2"] eval:arg]);
    
    return [super eval:arg];
}

@end
