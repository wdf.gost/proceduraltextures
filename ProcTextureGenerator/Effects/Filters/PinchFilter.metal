//
//  PinchFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "PinchFilter_params.h"



fragment float4 fshPinchFilter(ColorInOut in [[stage_in]],
                              texture2d<float, access::sample> bkgTexture [[ texture(0) ]],
                              constant PinchFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    
    float2 off = in.bkgCoord - float2(0.5);
    float d = length(off);
    float r = params.radius * params.pixel_step;
    d = mix(pow(d / r, -params.amount),
            1.0,
            float(d > r));
    
    float val = bkgTexture.sample(s, float2(0.5) + off * d).x;
    
    return float4(clamp(val, 0.0, 1.0));
}
