//
//  TwirlFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "TwirlFilter_params.h"



fragment float4 fshTwirlFilter(ColorInOut in [[stage_in]],
                               texture2d<float, access::sample> bkgTexture [[ texture(0) ]],
                               constant TwirlFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    
    float2 offset = in.bkgCoord - float2(0.5);
    float d = length(offset);
    float r = params.radius * params.pixel_step;
    
    float a = (1.0 - d / r) * params.angle;
    a = mix(a, 0.0, float(d > r));
    float ca = cos(a);
    float sa = sin(a);
    float newDx = ca * offset.x - sa * offset.y;
    float newDy = sa * offset.x + ca * offset.y;
    
    offset.x = newDx;
    offset.y = newDy;
    
    float val = bkgTexture.sample(s, float2(0.5) + offset).x;
    
    return float4(clamp(val, 0.0, 1.0));
}



