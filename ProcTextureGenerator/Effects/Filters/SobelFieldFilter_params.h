//
//  SobelFieldFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef SobelFieldFilter_params_h
#define SobelFieldFilter_params_h

typedef struct
{
    float pixel_step; // We assume ratio is 1.
} SobelFieldFilterParams;


#endif /* SobelFieldFilter_params_h */
