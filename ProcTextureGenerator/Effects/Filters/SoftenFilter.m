//
//  SoftenFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "SoftenFilter.h"
#import "SoftenFilter_params.h"



@implementation SoftenFilter
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"img"];
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshSoftenFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    SoftenFilterParams params;
    params.pixel_step = 1.0 / _imgTexture.width;
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(SoftenFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
