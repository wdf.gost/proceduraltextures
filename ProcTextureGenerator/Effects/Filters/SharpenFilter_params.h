//
//  SharpenFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef SharpenFilter_params_h
#define SharpenFilter_params_h

typedef struct
{
    float pixel_step; // We assume ratio is 1.
} SharpenFilterParams;


#endif /* SharpenFilter_params_h */
