//
//  SoftenFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "SoftenFilter_params.h"


/*
 0 1 2
 3 4 5
 6 7 8
 */

fragment float4 fshSoftenFilter(ColorInOut in [[stage_in]],
                               texture2d<float, access::sample> bkgTexture [[ texture(0) ]],
                               constant SoftenFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    const float ps = params.pixel_step;
    
    float p0 = bkgTexture.sample(s, in.bkgCoord + float2(-ps, -ps)).x;
    float p1 = bkgTexture.sample(s, in.bkgCoord + float2(  0, -ps)).x;
    float p2 = bkgTexture.sample(s, in.bkgCoord + float2(+ps, -ps)).x;
    
    float p3 = bkgTexture.sample(s, in.bkgCoord + float2(-ps,   0)).x;
    float p4 = bkgTexture.sample(s, in.bkgCoord + float2(  0,   0)).x;
    float p5 = bkgTexture.sample(s, in.bkgCoord + float2(+ps,   0)).x;
    
    float p6 = bkgTexture.sample(s, in.bkgCoord + float2(-ps,  ps)).x;
    float p7 = bkgTexture.sample(s, in.bkgCoord + float2(  0,  ps)).x;
    float p8 = bkgTexture.sample(s, in.bkgCoord + float2(+ps,  ps)).x;
    
    float val = ((p0 + p2 + p6 + p8) +
                 (p1 + p3 + p5 + p7) * 2.0 +
                 p4 * 4.0) / 16.0;
    
    return float4(clamp(val, 0.0, 1.0));
}
