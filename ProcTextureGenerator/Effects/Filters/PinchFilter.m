//
//  PinchFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "PinchFilter.h"
#import "PinchFilter_params.h"



@implementation PinchFilter
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"radius"];
    [self.inputs setValue:nil forKey:@"amount"];
    [self.inputs setValue:nil forKey:@"img"];
    _radius = @(100);
    _amount = @(1);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshPinchFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    PinchFilterParams params;
    params.pixel_step = 1.0 / _imgTexture.width;
    params.radius = [self getParamValue:@"radius"];
    params.amount = [self getParamValue:@"amount"];
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(PinchFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
