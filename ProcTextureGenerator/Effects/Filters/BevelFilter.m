//
//  BevelFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#import "BevelFilter.h"
#import "BevelFilter_params.h"



@implementation BevelFilter
{
    id<MTLTexture> _imgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"angleOffset"];
    [self.inputs setValue:nil forKey:@"img"];
    _angleOffset = @(0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshBevelFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    BevelFilterParams params;
    params.angle_offset = [self getParamValue:@"angleOffset"];
    params.angle_offset *= M_PI * 2.0;
    params.pixel_step = 1.0 / _imgTexture.width;
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(BevelFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"img"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"img"] eval:arg]);
    
    return [super eval:arg];
}

@end
