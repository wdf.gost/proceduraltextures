//
//  BevelFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 27/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef BevelFilter_params_h
#define BevelFilter_params_h

typedef struct
{
    float angle_offset;
    float pixel_step; // We assume ratio is 1.
} BevelFilterParams;


#endif /* BevelFilter_params_h */
