//
//  xDoGFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "xDoGFilter_params.h"



fragment float4 fshXDOGFilter(ColorInOut in [[stage_in]],
                                     texture2d<float, access::sample> g1Texture [[ texture(0) ]],
                                     texture2d<float, access::sample> g2Texture [[ texture(1) ]],
                                     constant xDoGFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    
    float g1 = g1Texture.sample(s, in.bkgCoord).x;
    float g2 = g2Texture.sample(s, in.bkgCoord).x;
    
    float dog = (1 + params.p) * g1 - params.p * g2;
    
    float val = 1 + tanh(params.phi * (dog - params.epsilon));
    val = mix(val, 1.0, float(dog >= params.epsilon));
    
    return float4(clamp(val, 0.0, 1.0));
}


