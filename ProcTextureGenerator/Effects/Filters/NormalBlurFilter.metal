//
//  NormalBlurFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "NormalBlurFilter_params.h"



fragment float4 fshNormalBlurFilter(ColorInOut in [[stage_in]],
                                    texture2d<float, access::sample> bkgTexture [[ texture(0) ]],
                                    texture2d<float, access::sample> mapTexture [[ texture(1) ]],
                                    constant NormalBlurFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    
    float2 origin = in.bkgCoord;
    float4 map = mapTexture.sample(s, origin);
    float2 dir = map.xy * params.pixel_step;
    float val = bkgTexture.sample(s, origin).x;
    float sigma = params.sigma + FLOAT_EPS;
    
    int r = int(2 * sigma) * int(map.w > FLOAT_EPS);
    float coef_sum = mix(1.0,
                         gauss_in_point(0, sigma),
                         float(map.w > FLOAT_EPS));
    val *= coef_sum;
    
    float2 cur_pos = origin + dir;
    float2 cur_neg = origin - dir;
    for (int i = 1; i <= r; ++i) {
        float cur_val_pos = bkgTexture.sample(s, cur_pos).x;
        float cur_val_neg = bkgTexture.sample(s, cur_neg).x;
        float cur_coef = gauss_in_point(i, sigma);
        
        val += (cur_val_pos + cur_val_neg) * cur_coef;
        coef_sum += 2.0 * cur_coef;
        cur_pos += mapTexture.sample(s, cur_pos).xy * params.pixel_step;
        cur_neg -= mapTexture.sample(s, cur_neg).xy * params.pixel_step;
    }
    val /= coef_sum;
    
    return float4(clamp(val, 0.0, 1.0));
}
