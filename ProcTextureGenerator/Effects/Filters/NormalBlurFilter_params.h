//
//  NormalBlurFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef NormalBlurFilter_params_h
#define NormalBlurFilter_params_h

typedef struct
{
    float sigma;
    float pixel_step; // We assume ratio is 1.
} NormalBlurFilterParams;


#endif /* NormalBlurFilter_params_h */
