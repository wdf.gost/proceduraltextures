//
//  TangentBlurFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef TangentBlurFilter_params_h
#define TangentBlurFilter_params_h

typedef struct
{
    float sigma;
    float pixel_step; // We assume ratio is 1.
} TangentBlurFilterParams;


#endif /* TangentBlurFilter_params_h */
