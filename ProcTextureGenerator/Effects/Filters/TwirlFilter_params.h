//
//  TwirlFilter_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 28/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#ifndef TwirlFilter_params_h
#define TwirlFilter_params_h

typedef struct
{
    float radius;
    float angle;
    float pixel_step; // We assume ratio is 1.
} TwirlFilterParams;


#endif /* TwirlFilter_params_h */
