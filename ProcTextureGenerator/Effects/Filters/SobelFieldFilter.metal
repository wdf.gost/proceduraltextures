//
//  SobelFieldFilter.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "SobelFieldFilter_params.h"



/*
 
 0 1 2
 3 4 5
 6 7 8
 
 */

fragment float4 fshSobelFieldFilter(ColorInOut in [[stage_in]],
                                    texture2d<float, access::sample> bkgTexture [[ texture(0) ]],
                                    constant SobelFieldFilterParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::clamp_to_edge,
                        filter::linear);
    
    float2 ox = float2(params.pixel_step, 0.0);
    float2 oy = float2(0.0, params.pixel_step);
    
    float p0 = bkgTexture.sample(s, in.bkgCoord - ox - oy).x;
    float p1 = bkgTexture.sample(s, in.bkgCoord - oy).x;
    float p2 = bkgTexture.sample(s, in.bkgCoord + ox - oy).x;
    float p3 = bkgTexture.sample(s, in.bkgCoord - ox).x;
    
    float p5 = bkgTexture.sample(s, in.bkgCoord + ox).x;
    float p6 = bkgTexture.sample(s, in.bkgCoord - ox + oy).x;
    float p7 = bkgTexture.sample(s, in.bkgCoord + oy).x;
    float p8 = bkgTexture.sample(s, in.bkgCoord + ox + oy).x;
    
    float dx = (p2 + 2.0 * p5 + p8) - (p0 + 2.0 * p3 + p6);
    float dy = (p6 + 2.0 * p7 + p8) - (p0 + 2.0 * p1 + p2);
    float2 normal = normalize(float2(dx, dy));    
    
    return float4(normal.x, normal.y, atan2(dy, dx) / M_PI, length(float2(dx, dy)));
}



