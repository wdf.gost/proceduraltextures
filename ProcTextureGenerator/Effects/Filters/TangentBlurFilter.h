//
//  TangentBlurFilter.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface TangentBlurFilter : TextureEffect

@property NSNumber *sigma;

@end

NS_ASSUME_NONNULL_END
