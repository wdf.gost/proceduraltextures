//
//  NormalBlurFilter.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "NormalBlurFilter.h"
#import "NormalBlurFilter_params.h"



@implementation NormalBlurFilter
{
    id<MTLTexture> _imgTexture;
    id<MTLTexture> _mapTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"imgImg"];
    [self.inputs setValue:nil forKey:@"imgMap"];
    [self.inputs setValue:nil forKey:@"sigma"];
    _sigma = @(0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshNormalBlurFilter"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    NormalBlurFilterParams params;
    params.pixel_step = 1.0 / _imgTexture.width;
    params.sigma = [self getParamValue:@"sigma"];
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(NormalBlurFilterParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_imgTexture
                              atIndex:0];
    [renderEncoder setFragmentTexture:_mapTexture
                              atIndex:1];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _imgTexture = (self.inputs[@"imgImg"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"imgImg"] eval:arg]);
    
    _mapTexture = (self.inputs[@"imgMap"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"imgMap"] eval:arg]);
    
    return [super eval:arg];
}

@end
