//
//  xDoGFilter.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 09/02/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "TextureEffect.h"

NS_ASSUME_NONNULL_BEGIN

@interface xDoGFilter : TextureEffect

@property NSNumber *epsilon;
@property NSNumber *phi;
@property NSNumber *p;

@end

NS_ASSUME_NONNULL_END
