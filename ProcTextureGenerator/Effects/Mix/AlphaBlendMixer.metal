//
//  AlphaBlendMixer.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include "../TECommon.metal"
#include "AlphaBlendMixer_params.h"



template<typename T>
inline static T blendMix(T src, T dst, float a) {
    return mix(dst, src, a);
}


template<typename T>
inline static T blendMax(T src, T dst, float a) {
    float val = max(dst, src);
    return mix(dst, val, a);
}


template<typename T>
inline static T blendMin(T src, T dst, float a) {
    float val = min(dst, src);
    return mix(dst, val, a);
}


template<typename T>
inline static T blendSum(T src, T dst, float a) {
    float val = clamp(0.0, 1.0, src + dst);
    return mix(dst, val, a);
}


template<typename T>
inline static T blendDif(T src, T dst, float a) {
    float val = clamp(0.0, 1.0, abs(src - dst));
    return mix(dst, val, a);
}


template<typename T>
inline static T blendMul(T src, T dst, float a) {
    float val = clamp(0.0, 1.0, src * dst);
    return mix(dst, val, a);
}


template<typename T>
inline static T blendDiv(T src, T dst, float a) {
    float val = clamp(0.0, 1.0, src / dst);
    return mix(dst, val, a);
}


template<typename T>
inline static T blendSub(T src, T dst, float a) {
    float val = clamp(0.0, 1.0, src - dst);
    return mix(dst, val, a);
}



template<typename T>
T blend(T src, T dst, float a, BlendMode blendType) {
    T res = 0.0;
    switch (blendType) {
        case BLEND_MIX:
            res = blendMix(src, dst, a);
            break;
        case BLEND_MAX:
            res = blendMax(src, dst, a);
            break;
        case BLEND_MIN:
            res = blendMin(src, dst, a);
            break;
        case BLEND_SUM:
            res = blendSum(src, dst, a);
            break;
        case BLEND_DIF:
            res = blendDif(src, dst, a);
            break;
        case BLEND_MUL:
            res = blendMul(src, dst, a);
            break;
        case BLEND_DIV:
            res = blendDiv(src, dst, a);
            break;
        case BLEND_SUB:
            res = blendSub(src, dst, a);
            break;
        default:
            res = src;
            break;
    };
    return res;
}



fragment float4 fshAlphaBlendMixer(ColorInOut in [[stage_in]],
                                   texture2d<float, access::sample> frgTexture [[ texture(0) ]],
                                   texture2d<float, access::sample> bkgTexture [[ texture(1) ]],
                                   constant AlphaBlendMixerParams &params [[ buffer(0) ]])
{
    constexpr sampler s(address::repeat,
                        filter::linear);
    float bkg = bkgTexture.sample(s, in.bkgCoord).x;
    float frg = frgTexture.sample(s, in.texCoord).x;
    
    float val = blend(frg, bkg, params.alpha, params.blendMode);
    return float4(clamp(val, 0.0, 1.0));
}


