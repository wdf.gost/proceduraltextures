//
//  AlphaBlendMixer.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "../TextureEffect.h"



NS_ASSUME_NONNULL_BEGIN

@interface AlphaBlendMixer : TextureEffect

@property        NSNumber         *alpha;
@property        NSNumber         *blendMode;

@end

NS_ASSUME_NONNULL_END
