//
//  AlphaBlendMixer.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import "AlphaBlendMixer.h"
#import "AlphaBlendMixer_params.h"

@implementation AlphaBlendMixer
{
    id<MTLTexture> _frgTexture;
    id<MTLTexture> _bkgTexture;
}



- (void)initInputs {
    [super initInputs];
    [self.inputs setValue:nil forKey:@"alpha"];
    [self.inputs setValue:nil forKey:@"blendMode"];
    [self.inputs setValue:nil forKey:@"imgFrg"];
    [self.inputs setValue:nil forKey:@"imgBkg"];
    _alpha = @(1);
    _blendMode = @(0);
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshAlphaBlendMixer"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    AlphaBlendMixerParams params;
    params.alpha = [self getParamValue:@"alpha"];
    params.blendMode = [self getParamValue:@"blendMode"];
    [renderEncoder setFragmentBytes:&params
                             length:sizeof(AlphaBlendMixerParams)
                            atIndex:0];
    
    [renderEncoder setFragmentTexture:_frgTexture
                              atIndex:0];
    [renderEncoder setFragmentTexture:_bkgTexture
                              atIndex:1];
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    _frgTexture = (self.inputs[@"imgFrg"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"imgFrg"] eval:arg]);
    _bkgTexture = (self.inputs[@"imgBkg"] == nil ?
                   self.stubTexture :
                   [self.inputs[@"imgBkg"] eval:arg]);
    
    return [super eval:arg];
}

@end
