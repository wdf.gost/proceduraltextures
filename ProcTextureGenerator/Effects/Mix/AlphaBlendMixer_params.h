//
//  AlphaBlendMixer_params.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#ifndef AlphaBlendMixer_params_h
#define AlphaBlendMixer_params_h




typedef enum {
    BLEND_MIX,
    BLEND_MAX,
    BLEND_MIN,
    BLEND_SUM,
    BLEND_DIF,
    BLEND_MUL,
    BLEND_DIV,
    BLEND_SUB,
    
    BLEND_COUNT
} BlendMode;



typedef struct
{
    float alpha;
    BlendMode blendMode;
} AlphaBlendMixerParams;


#endif /* AlphaBlendMixer_params_h */
