//
//  TextureEffect
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <simd/simd.h>



#import "TextureEffect.h"



static const float DEFAULT_VTC[8] = { 0,  1,    1,  1,    0,  0,    1,  0};
static const float DEFAULT_VTP[8] = {-1,  1,    1,  1,   -1, -1,    1, -1};
static const size_t DEFAULT_COORDS_SIZE = sizeof(float) * 8;
static const NSUInteger TEXTURE_SIZE = 1024;



@implementation TextureEffect
{
    id<MTLDevice> _device;
    
    id <MTLRenderPipelineState> _pipelineState;
    
    id<MTLBuffer> _vBufPosition;
    id<MTLBuffer> _vBufTexture;
    
    id<MTLTexture> _stubTexture;
}



- (instancetype)initWithDevice:(id<MTLDevice>)device {
    self = [super init];
    if (self) {
        _device = device;
        
        id<MTLLibrary> defaultLibrary = [_device newDefaultLibrary];
        id <MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vshTextureEffect"];
        id <MTLFunction> fragmentFunction = [self loadFragmentFunctionFromLibrary:defaultLibrary];
        
        MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        pipelineStateDescriptor.label = @"TextureEffect Pipeline";
        pipelineStateDescriptor.vertexFunction = vertexFunction;
        pipelineStateDescriptor.fragmentFunction = fragmentFunction;
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatRGBA32Float;
        pipelineStateDescriptor.colorAttachments[0].blendingEnabled = NO;
        
        
        MTLResourceOptions resourceOptions = (MTLResourceOptionCPUCacheModeDefault |
                                              MTLStorageModeShared);
        _vBufPosition = [_device newBufferWithBytes:DEFAULT_VTP
                                             length:DEFAULT_COORDS_SIZE
                                            options:resourceOptions];
        _vBufTexture  = [_device newBufferWithBytes:DEFAULT_VTC
                                             length:DEFAULT_COORDS_SIZE
                                            options:resourceOptions];
        
        
        MTLTextureDescriptor *textureDescriptor =
        [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA32Float
                                                           width:1
                                                          height:1
                                                       mipmapped:NO];
        textureDescriptor.storageMode = MTLStorageModePrivate;
        textureDescriptor.usage       = MTLTextureUsageShaderRead;
        _stubTexture = [device newTextureWithDescriptor:textureDescriptor];
        
        NSError *error = NULL;
        _pipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
        if (!_pipelineState)
        {
            NSLog(@"Failed to created pipeline state, error %@", error);
        }
    }
    
    return self;
}



- (id <MTLFunction>)loadFragmentFunctionFromLibrary:(id<MTLLibrary>)library {
    return [library newFunctionWithName:@"fshTextureEffect"];
}



- (void)setupCustomParams:(id<MTLRenderCommandEncoder>)renderEncoder {
    NSAssert(NO, @"Don't use base class.");
}



- (id<MTLTexture>)createEmptyTexture {
    MTLTextureDescriptor *textureDescriptor =
    [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA32Float
                                                       width:TEXTURE_SIZE
                                                      height:TEXTURE_SIZE
                                                   mipmapped:NO];
    textureDescriptor.storageMode = MTLStorageModeManaged;
    textureDescriptor.usage       = (MTLTextureUsageShaderRead |
                                     MTLTextureUsageShaderWrite |
                                     MTLTextureUsageRenderTarget);
    
    return [_device newTextureWithDescriptor:textureDescriptor];
}



- (void)applyTo:(id<MTLTexture>)texture
       inCmdBuf:(id<MTLCommandBuffer>)commandBuffer
      transform:(NSDictionary *)transform
{
    
    MTLRenderPassDescriptor *renderPassDesc = [MTLRenderPassDescriptor renderPassDescriptor];
    renderPassDesc.colorAttachments[0].texture = texture;
    renderPassDesc.colorAttachments[0].loadAction  = MTLLoadActionLoad;
    renderPassDesc.colorAttachments[0].storeAction = MTLStoreActionStore;
    id <MTLRenderCommandEncoder> renderEncoder =
    [commandBuffer renderCommandEncoderWithDescriptor:renderPassDesc];
    renderEncoder.label = @"TextureEffect encoder";
    
    [renderEncoder pushDebugGroup:@"Apply effect"];
    [renderEncoder setRenderPipelineState:_pipelineState];
    
    [renderEncoder setVertexBuffer:_vBufPosition
                            offset:0
                           atIndex:0];
    [renderEncoder setVertexBuffer:_vBufTexture
                            offset:0
                           atIndex:1];
    
    matrix_float3x3 t_mat = getTransformMat(transform);
    [renderEncoder setVertexBytes:&t_mat
                           length:sizeof(matrix_float3x3)
                          atIndex:2];

    [self setupCustomParams:renderEncoder];
    
    [renderEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip
                      vertexStart:0
                      vertexCount:4];
    
    [renderEncoder popDebugGroup];
    [renderEncoder endEncoding];
}



#pragma mark - Matrix Math Utilities

static matrix_float3x3 matrix3x3_translation(float tx, float ty)
{
    return (matrix_float3x3) {{
        { 1,   0,  0},
        { 0,   1,  0},
        { tx,  ty, 1}
    }};
}

static matrix_float3x3 matrix3x3_scale(float kx, float ky)
{
    return (matrix_float3x3) {{
        {kx,  0, 0},
        { 0, ky, 0},
        { 0,  0, 1}
    }};
}



static matrix_float3x3 matrix3x3_rotation(float radians)
{
    float ct = cosf(radians);
    float st = sinf(radians);
    
    return (matrix_float3x3) {{
        { ct,-st, 0},
        { st, ct, 0},
        { 0,   0, 1}
    }};
}



static matrix_float3x3 getTransformMat(NSDictionary *transform) {
    matrix_float3x3 rot_mat = matrix3x3_rotation([transform[@"a"] floatValue] * M_PI);
    matrix_float3x3 trs_mat = matrix3x3_translation([transform[@"tx"] floatValue],
                                                    [transform[@"ty"] floatValue]);
    matrix_float3x3 scl_mat = matrix3x3_scale(1.0 / [transform[@"sx"] floatValue],
                                              1.0 / [transform[@"sy"] floatValue]);
    
    
    matrix_float3x3 trs_centre_f_mat = matrix3x3_translation(-0.5, -0.5);
    matrix_float3x3 trs_centre_b_mat = matrix3x3_translation( 0.5,  0.5);
    
    matrix_float3x3 tmat = trs_centre_f_mat;
    tmat = matrix_multiply(rot_mat, tmat);
    tmat = matrix_multiply(scl_mat, tmat);
    tmat = matrix_multiply(trs_mat, tmat);
    tmat = matrix_multiply(trs_centre_b_mat, tmat);
    
    return  tmat;
}



#pragma mark - Graph node functions

- (void)initInputs {
    [self.inputs setValue:nil forKey:@"transform"];
}



- (float)getParamValue:(NSString *)key {
    return (self.inputs[key] == nil ?
            [[self valueForKey:key] floatValue] :
            [[self.inputs[key] eval:nil] floatValue]);
}



- (id<MTLTexture>)stubTexture {
    return  _stubTexture;
}



- (id)eval:(id)arg {
    id result = [self cachedValue];
    if (result != nil) {
        return result;
    }
    
    NSDictionary *transform = (self.inputs[@"transform"] == nil ?
                               @{@"a":@(0),
                                 @"tx":@(0),
                                 @"ty":@(0),
                                 @"sx":@(1),
                                 @"sy":@(1)} :
                               [self.inputs[@"transform"] eval:arg]);
    
    result = [self createEmptyTexture];
    [self applyTo:result
         inCmdBuf:arg
        transform:transform];
    
    [self setCachedValue:result];
    return result;
}


@end
