//
//  TextureEffect.metal
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//

#include <metal_stdlib>

#include "TECommon.metal"


using namespace metal;



vertex ColorInOut vshTextureEffect(constant float2 *vtp  [[ buffer(0) ]],
                                   constant float2 *vtc  [[ buffer(1) ]],
                                   constant matrix_float3x3 &transform  [[ buffer(2) ]],
                                   uint vid [[ vertex_id ]])
{
    ColorInOut out;
    
    float4 position = float4(vtp[vid].x, -vtp[vid].y, 0.0, 1.0);
    out.position = position;
    out.texCoord = (transform * float3(vtc[vid], 1.0)).xy;
    out.bkgCoord = vtc[vid];
    
    return out;
}



fragment float4 fshTextureEffect(ColorInOut in [[stage_in]],
                                 texture2d<float, access::sample> bkgTexture [[ texture(0) ]])
{
    constexpr sampler s(address::clamp_to_zero,
                        filter::nearest);
    float back = bkgTexture.sample(s, in.bkgCoord).x;
    return float4(float3(back), 1.0);
}
