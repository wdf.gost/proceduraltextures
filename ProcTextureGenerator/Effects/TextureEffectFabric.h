//
//  TextureEffectFabric.h
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 12/01/2019.
//  Copyright © 2019 George Ostrobrod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Graph/GraphNode.h"

NS_ASSUME_NONNULL_BEGIN



@interface TextureEffectFabric : NSObject

+ (GraphNode *)createTextureEffectByName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
