//
//  AppDelegate.m
//  ProcTextureGenerator
//
//  Created by George Ostrobrod on 26/11/2018.
//  Copyright © 2018 George Ostrobrod. All rights reserved.
//
#import <Accelerate/Accelerate.h>

#import "AppDelegate.h"



@implementation AppDelegate
{
    NSMutableDictionary<NSNumber *, GraphNode *> *_processingGraph;
    NSNumber *_processingGraphRootID;
    
    id<MTLTexture> _texture;
    
    NSString *_openedFile;
}



- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _processingGraph = [NSMutableDictionary new];
}



- (void)applicationWillTerminate:(NSNotification *)aNotification {
}



- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
    return YES;
}



#pragma mark - Graph controller

- (void)addNode:(GraphNode *)node atKey:(NSNumber *)key {
    _processingGraph[key] = node;
}



- (void)addRootNode:(GraphNode *)node atKey:(NSNumber *)key {
    if (node == nil) {
        return;
    }
    [self addNode:node atKey:key];
    _processingGraphRootID = key;
}



- (void)removeNodeAtKey:(NSNumber *)key {
    [_processingGraph[_processingGraphRootID] checkAndResetDependedOn:_processingGraph[key]];
    [_processingGraph removeObjectForKey:key];
}



- (void)addConnectionToNode:(NSNumber *)dstID
                      input:(NSString *)dstInput
                   fromNode:(NSNumber *)srcID
{
    [_processingGraph[dstID] addConnectionFromNode:_processingGraph[srcID]
                                           toInput:dstInput];
    [_processingGraph[_processingGraphRootID] checkAndResetDependedOn:_processingGraph[srcID]];
}



- (void)removeConnectionFromNode:(NSNumber *)dstID
                           input:(NSString *)dstInput
{
    [_processingGraph[dstID] removeConnectionAtInput:dstInput];
    [_processingGraph[_processingGraphRootID] checkAndResetDependedOn:_processingGraph[dstID]];
}



- (void)setValue:(NSNumber *)value
          toNode:(NSNumber *)key
           input:(NSString *)input
{
    [_processingGraph[key] setValue:value
                             forKey:input];
    [_processingGraph[_processingGraphRootID] checkAndResetDependedOn:_processingGraph[key]];
}



- (id)computeGraphIn:(id<MTLCommandBuffer>)cmdBuf {
    if (_processingGraphRootID == nil) {
        return nil;
    }
    @synchronized(_texture) {
        _texture = [_processingGraph[_processingGraphRootID] eval:cmdBuf];
    }
    return _texture;
}



-(NSImage *)getTextureImage {
    vImage_Buffer buf_float = {
        .data     = malloc((vImagePixelCount)(_texture.width * _texture.height * 4)),
        .width    = (vImagePixelCount)_texture.width,
        .height   = (vImagePixelCount)_texture.height,
        .rowBytes = (vImagePixelCount)_texture.width * 4
    };
    
    @synchronized(_texture) {
        
        id <MTLCommandBuffer> commandBuffer = [[MTLCreateSystemDefaultDevice() newCommandQueue] commandBuffer];
        if (_texture) {
            id<MTLBlitCommandEncoder> syncTexture = [commandBuffer blitCommandEncoder];
            [syncTexture synchronizeTexture:_texture slice:0 level:0];
            [syncTexture endEncoding];
        }
        [commandBuffer commit];
        [commandBuffer waitUntilCompleted];
        
        
        [_texture getBytes:buf_float.data
               bytesPerRow:buf_float.rowBytes
                fromRegion:MTLRegionMake2D(0,
                                           0,
                                           _texture.width,
                                           _texture.height)
               mipmapLevel:0];
    }
    
    vImage_Buffer buf = {
        .data     = malloc((vImagePixelCount)(_texture.width * _texture.height)),
        .width    = (vImagePixelCount)_texture.width,
        .height   = (vImagePixelCount)_texture.height,
        .rowBytes = (vImagePixelCount)_texture.width
    };
    for (int y = 0; y < _texture.height; ++y) {
        uint8_t *dst_line = (uint8_t *)buf.data + y * buf.rowBytes;
        float *src_line = (float *)((uint8_t *)buf_float.data + y * buf_float.rowBytes);
        for (int x = 0; x < _texture.height; ++x) {
            dst_line[x] = 255 * src_line[x];
        }
    }
    
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    NSData* destinationImageData = [[NSData alloc] initWithBytes:buf.data
                                                          length:buf.height * buf.rowBytes];
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((__bridge CFDataRef)(destinationImageData));
    CGImageRef finalImageRef = CGImageCreate(buf.width,
                                             buf.height,
                                             8,
                                             8,
                                             buf.rowBytes,
                                             colorSpace,
                                             (CGBitmapInfo)kCGImageAlphaNone,
                                             dataProvider,
                                             NULL,
                                             NO,
                                             kCGRenderingIntentDefault);
    CGColorSpaceRelease(colorSpace);
    CGDataProviderRelease(dataProvider);
    NSImage *img = [[NSImage alloc] initWithCGImage:finalImageRef
                                               size:NSMakeSize(buf.width, buf.height)];
    CGImageRelease(finalImageRef);
    
    free(buf_float.data);
    free(buf.data);
    
    return img;
}



- (IBAction)onExportImage:(id)sender {
    NSSavePanel*    panel = [NSSavePanel savePanel];
    [panel setNameFieldStringValue:@"texture.png"];
    
    if ([panel runModal] != NSModalResponseOK) {
        return;
    }

    NSURL*  theFile = [panel URL];

    NSImage *image = [self getTextureImage];

    NSString *path =theFile.path;

    NSData *imageData = [image TIFFRepresentation];
    NSBitmapImageRep *imageRep = [NSBitmapImageRep imageRepWithData:imageData];
    NSDictionary *imageProps = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0]
                                                           forKey:NSImageCompressionFactor];
    imageData = [imageRep representationUsingType:NSBitmapImageFileTypePNG
                                       properties:imageProps];

    NSError *error = nil;
    [imageData writeToFile:path
                   options:NSDataWritingAtomic
                     error:&error];
    if (error) {
        NSLog(@"Fail: %@", [error localizedDescription]);
    }
}



- (IBAction)onSaveGraph:(id)sender {
    [_graphController requestGraphJSON];
}



- (IBAction)onLoadGraph:(id)sender {
    NSOpenPanel *openDlg = [NSOpenPanel openPanel];
    openDlg.canChooseFiles = YES;
    openDlg.canChooseDirectories = NO;
    openDlg.allowsMultipleSelection = NO;
    openDlg.allowedFileTypes = @[@"json", @"jsn", @"txt"];
    
    if ([openDlg runModal] != NSModalResponseOK) {
        return;
    }
    
    NSError *error;
    NSString *json = [NSString stringWithContentsOfURL:[openDlg URLs][0] encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"Fail: %@", [error localizedDescription]);
    }
    [_graphController setGraphJSON:json];
    _openedFile = [openDlg URLs][0].path;
}



- (void)updateGraph {
    [_processingGraph[_processingGraphRootID] resetNode];
}



- (IBAction)onNewGraph:(id)sender {
    [_graphController clearGraph];
    _openedFile = nil;
}



- (void)saveGraphJSON:(NSString *)json {
    
    NSString *path = _openedFile;
    
    if (path == nil) {
        NSSavePanel* panel = [NSSavePanel savePanel];
        [panel setNameFieldStringValue:@"graph.json"];
        if ([panel runModal] != NSModalResponseOK) {
            return;
        }
        path = [panel URL].path;
    }
    
    NSError *error;
    [json writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"Fail: %@", [error localizedDescription]);
    }
    _openedFile = path;
}

@end
