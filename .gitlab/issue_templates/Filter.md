### Description
<!--- Description -->

### Result example
<!--- image -->

### Inputs
* `transform` - a trasnform object modifies result space.
* <param 1>
* <param 2>
* <param 3>

### Outputs
* `image` - an image object contains the filter result.

### Filter implementation checklist
- [ ] `<FilterName>.m`
    - [ ] `initInputs`
    - [ ] `loadFragmentFunctionFromLibrary`
    - [ ] `setupCustomParams`
    - [ ] `eval`
- [ ] `<FilterName>.h` - numeric input params properties.
- [ ] `<FilterName>.metal`
    - [ ] includes of common and parameters headers.
    - [ ] fragment shader kernel.
- [ ] `<FilterName>_params.h` - numeric inputs structure.
- [ ] `TextureEffectFabric.m`
- [ ] `flow-types.js`

